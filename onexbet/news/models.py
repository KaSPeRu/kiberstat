from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


LEVEL_CHOICES = (
    (0, _('info')),
    (10, _('warning')),
)


class AlertMessageManager(models.Manager):
    def get_alert(self):
        queryset = self.get_queryset()

        now = timezone.now()

        alert = queryset.filter(start_date__lte=now, end_date__gte=now, is_active=True)

        if len(alert):
            return alert[0]
        else:
            return None


class AlertMessage(models.Model):
    title = models.TextField(_('alert title'))
    level = models.PositiveSmallIntegerField(_('level'), choices=LEVEL_CHOICES)
    start_date = models.DateTimeField(_('start date'))
    end_date = models.DateTimeField(_('end date'))
    is_active = models.BooleanField(_('is active'), default=True)

    objects = AlertMessageManager()

    class Meta:
        ordering = ['-start_date']
        verbose_name = _('alert message')
        verbose_name_plural = _('alert messages')

    def __str__(self):
        return self.title