import pprint
import datetime
from django.contrib import admin
from news.models import AlertMessage


@admin.register(AlertMessage)
class AlertMessageAdmin(admin.ModelAdmin):
    list_display = ('title', 'level', 'start_date', 'end_date', 'is_active')
    date_hierarchy = 'start_date'
    list_filter = (
        'is_active',
        'level',
    )
    search_fields = ('title', )
