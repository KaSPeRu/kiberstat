from django import template
from django.utils.translation import ugettext_lazy as _
from news.models import AlertMessage


register = template.Library()


@register.inclusion_tag('alert_message.html', takes_context=True)
def alert_message(context):
    context['alert_message'] = AlertMessage.objects.get_alert()
    return context
