import decimal
from django import template
from mortalkombat.models import Teams

register = template.Library()


def format_number(num):
    try:
        dec = decimal.Decimal(num)
    except:
        return '0'
    tup = dec.as_tuple()
    delta = len(tup.digits) + tup.exponent
    digits = ''.join(str(d) for d in tup.digits)
    if delta <= 0:
        zeros = abs(tup.exponent) - len(tup.digits)
        val = '0.' + ('0' * zeros) + digits
    else:
        val = digits[:delta] + ('0' * tup.exponent) + '.' + digits[delta:]
    val = val.rstrip('0')
    if val[-1] == '.':
        val = val[:-1]
    if tup.sign:
        return '-' + val

    if '.' in val:
        try:
            val = float(val)
        except ValueError:
            val = 0
    else:
        try:
            val = int(val)
        except ValueError:
            val = 0

    return val
def format_number2(num):
    try:
        dec = decimal.Decimal(num)
    except:
        return '0'
    tup = dec.as_tuple()
    delta = len(tup.digits) + tup.exponent
    digits = ''.join(str(d) for d in tup.digits)
    if delta <= 0:
        zeros = abs(tup.exponent) - len(tup.digits)
        val = '0.' + ('0' * zeros) + digits
    else:
        val = digits[:delta] + ('0' * tup.exponent) + '.' + digits[delta:]
    val = val.rstrip('0')
    if val[-1] == '.':
        val = val[:-1]
    if tup.sign:
        return '-' + val
    return val

@register.filter
def teamvalue(value, arg):
    result = getattr(value, arg)
    if 'result_round' not in arg:
        result = format_number(result)
    if result is None:
        result = ''
    # if (arg == 'net_victory_total' or arg == 'result_brutality' or arg == 'result_fatality') and result:
        # result = int(result)
    if result == 0:
        result = ''

    return result

@register.filter
def round_value(value, arg):

    result = getattr(value, arg)
    if result is None:
        result = ''

    return result

@register.filter
def round_total_value(value, arg):

    result = value.get_result_round_total_account(arg)
    if result is None:
        result = ''

    return result

@register.filter
def formatnumber(value):
    result = format_number(value)
    if result is None:
        result = ''
    if result == 0:
        result = ''
    return result

@register.filter
def formatnumber2(value):
    result = format_number2(value)
    return result.replace(',', '.')

@register.filter
def checkkey(value, arg):
    return arg.get('q_{}'.format(value))

@register.filter
def dyeparam(team, key):

    result = ''

    param = getattr(team, key)

    if key == 'first_1x2_win' and team.is_win_first():
        result = 'win-color'

    if key == 'second_1x2_win' and team.is_win_second():
        result = 'win-color'

    if key == 'first_win' and param and team.has_win_first_round_first():
        result = 'win-first-round'

    if key == 'second_win' and param and team.has_win_first_round_second():
        result = 'win-first-round'

    if 'result_round' in key and param and 'B' in param.name:
        result = 'brutality-color'

    if 'result_round' in key and param and 'F' in param.name:
        result = 'fatality-color'

    # result = getattr(value, arg)

    return result

@register.filter
def outsider_win_filter(team, key):

    result = ''
    param = getattr(team, key)

    if param and team.outsider_win_in_round(param.name) is True:
        result = 'outsider_win'

    return result

@register.filter
def round_win_filter(team, key):

    result = ''
    param = getattr(team, key)
    if param is None:
        pass
    elif '1:0' in param.name:
        result = 'round_win_first'
    elif '0:1' in param.name:
        result = 'round_win_second'

    return result


@register.simple_tag
def get_current_game():
    return Teams.objects.last_game()
