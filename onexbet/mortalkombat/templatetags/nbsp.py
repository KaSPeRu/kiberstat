from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter()
def nbsp(value):
    result = '&nbsp;'.join(value.split(' '))
    result = '&#8209;'.join(result.split('-'))
    return mark_safe(result)
