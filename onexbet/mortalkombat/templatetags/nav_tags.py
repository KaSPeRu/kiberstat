from django import template
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from mortalkombat.models import BetMirror


register = template.Library()

@register.inclusion_tag('nav_mainmenu.html', takes_context=True)
def nav_mainmenu(context):
    item_list = {}
    user = context.get('user')
    for group_name, menu_list in settings.NAV_MAINMENU.items():
        item_list[group_name] = []
        for url_name, menu in menu_list.items():
            if user.has_perms(menu.get('permission_required', [])):
                item_list[group_name].append({
                    'url_name': url_name,
                    'title': _(menu.get('title')),
                })
    context['menu'] = item_list
    return context

@register.inclusion_tag('nav_usermenu.html', takes_context=True)
def nav_usermenu(context):
    return context


@register.inclusion_tag('nav_betmirror.html', takes_context=True)
def nav_betmirror(context):
    context['betmirror'] = BetMirror.objects.last_url()
    return context
