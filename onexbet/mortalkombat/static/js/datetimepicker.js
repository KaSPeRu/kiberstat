    $(function() {
        if (LANGUAGE_CODE == undefined) {
            LANGUAGE_CODE = 'en';
        }

        $('#date_game').datetimepicker({
            locale: LANGUAGE_CODE,
            format: 'DD.MM.YY',
            showClear: true,
            showClose: true,
        });
        $('#time_game').datetimepicker({
            locale: LANGUAGE_CODE,
            format: 'HH:mm',
            showClear: true,
            showClose: true,
        });
    });