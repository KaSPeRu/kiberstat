    $(function() {
        function scaleUp(element_class) {
            var table_element = $(element_class);
            var page = table_element.attr("data-scale");
            var scale = localStorage.getItem(page);

            if (scale) {
                var fontSize = parseInt(scale);
            } else {
                var fontSize = parseInt(table_element.css("font-size"));
            }
            
            fontSize = fontSize + 1 + "px";
            $(element_class).css({ "font-size": fontSize });
            localStorage.setItem(page, '' + fontSize);
        }

        function scaleDown(element_class) {

            var table_element = $(element_class);
            var page = table_element.attr("data-scale");
            var scale = localStorage.getItem(page);

            if (scale) {
                var fontSize = parseInt(scale);
            } else {
                var fontSize = parseInt(table_element.css("font-size"));
            }

            if (fontSize > 10) {
                fontSize = fontSize - 1 + "px";
                $(element_class).css({ "font-size": fontSize });
                localStorage.setItem(page, '' + fontSize);
            }
        }

        var element_class = '.scale'
        var table_element = $(element_class);
        var page = table_element.attr("data-scale");
        var scale = localStorage.getItem(page);
                
        if (scale) {
            $(element_class).css({ "font-size": scale });
        }
        $('#scaleUpButton').click(function () {
            scaleUp(element_class);
        });
        $('#scaleDownButton').click(function () {
            scaleDown(element_class)
        });            

    });