from django.views.generic import TemplateView
from mortalkombat.models import Fighter, Teams
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import translation
from django.contrib.auth.mixins import PermissionRequiredMixin

class StatPairView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'mortalkombat/statpair.html'
    login_url = '/login/'
    redirect_field_name = 'next'
    search_query = {}
    permission_required = 'mortalkombat.view_stat_pair'

    def get_query_string(self):
        query_string_list = []
        if self.request.META['QUERY_STRING']:
            search_query_keys = self.search_query.keys()
            for param in self.request.META['QUERY_STRING'].split('&'):
                if 'page' not in param and param not in search_query_keys:
                    query_string_list.append(param)
        query_string = '&'.join(query_string_list)
        return query_string


    def get_context_data(self, **kwargs):

        fighter_one = self.request.GET.get('fighter_one')
        fighter_two = self.request.GET.get('fighter_two')

        if fighter_one is None and self.request.session.get('fighter_one') is not None:
            fighter_one = self.request.session.get('fighter_one')
        elif fighter_one is not None:
            self.request.session['fighter_one'] = fighter_one

        if fighter_two is None and self.request.session.get('fighter_two') is not None:
            fighter_two = self.request.session.get('fighter_two')
        elif fighter_two is not None:
            self.request.session['fighter_two'] = fighter_two


        self.search_query['fighter_one'] = fighter_one
        self.search_query['fighter_two'] = fighter_two
        kwargs['result'], kwargs['total'], kwargs['total_all'] = Teams.objects.get_stat_pair(
            fighter_one, fighter_two)
        kwargs['query_string'] = self.get_query_string()
        kwargs['search_query'] = self.search_query

        curent_language = translation.get_language()

        fighter_list = Fighter.objects.filter(is_active=True)
        if curent_language == 'ru':
            fighter_list = fighter_list.order_by('name')
        else:
            fighter_list = fighter_list.order_by('name_en')


        kwargs['fighter_list'] = fighter_list

        return super().get_context_data(**kwargs)
