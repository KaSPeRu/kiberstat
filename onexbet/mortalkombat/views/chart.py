import datetime
from django.views.generic import TemplateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from mortalkombat.models import Teams
from django.db.models import F


class ChartView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'mortalkombat/chart.html'
    login_url = '/login/'
    redirect_field_name = 'next'
    search_query = {}
    permission_required = 'mortalkombat.view_stat_pair'
    search_query = {}

    def get_context_data(self, **kwargs):
        result = {}
        date_game = self.request.GET.get('dg')

        if date_game is None and self.request.session.get('date_game') is not None:
            date_game = self.request.session.get('date_game')
        elif date_game is not None:
            self.request.session['date_game'] = date_game

        if date_game:
            try:
                # Convert str date to datetime
                dategame = datetime.datetime.strptime(
                    date_game, '%d.%m.%y').date()
                # add to search_query
                # print(dategame)
                queryset = Teams.objects.filter(
                    is_active=True,
                    finish=True,
                    date_game__date=dategame).order_by('date_game')

                queryset = queryset.annotate(total_score=F(
                    'score_first') + F('score_second'))


                if queryset.count() > 0:

                    result['t'] = []
                    result['r'] = []
                    result['f'] = []
                    result['b'] = []
                    result['ts'] = []


                    for teams in queryset.all():
                        r, b, f = 0, 0, 0
                        result['t'].append(teams.date_game)
                        if teams.has_r():
                            r = teams.without_finishing
                        elif teams.without_finishing:
                            r = teams.without_finishing * -1
                        else:
                            r = 0

                        if teams.has_b():
                            b = teams.brutality
                        elif teams.brutality:
                            b = teams.brutality * -1
                        else:
                            b = 0

                        if teams.has_f():
                            f = teams.fatality
                        elif teams.fatality:
                            f = teams.fatality  * -1
                        else:
                            f = 0

                        result['r'].append(r)
                        result['f'].append(f)
                        result['b'].append(b)
                        result['ts'].append(teams.total_score)


                self.search_query['dg'] = date_game
            except ValueError as e:
                self.search_query['dg'] = None
        else:
            # add to search_query
            self.search_query['dg'] = None

        kwargs['result'] = result
        kwargs['search_query'] = self.search_query

        return super().get_context_data(**kwargs)
