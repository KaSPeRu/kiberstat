import datetime
import copy
import decimal
from django.views.generic import ListView
from django.utils.translation import ugettext_lazy as _
from mortalkombat.models import Teams, Fighter, ResultRound
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.utils import translation
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import F
from django.shortcuts import redirect

STAT_PP = 50
STAT_PP_LIST = ['50', '70', '100', '150', '300']
SCORE_LIST = [0, 1, 2, 3, 4, 5]
TOTAL_SCORE_LIST = [5, 6, 7, 8, 9]
RESULT_ROUND_OTHER = [
    {'slug': 'rrw1', 'name': _('W1'), 'query': '1:0'},
    {'slug': 'rrwr2', 'name': _('W2'), 'query': '0:1'},
    {'slug': 'rrb', 'name': _('B'), 'query': 'B'},
    {'slug': 'rrf', 'name': _('F'), 'query': 'F'},
    {'slug': 'rrr', 'name': _('R'), 'query': 'R'},
]

COLUMNS = {
    'nvt': {'field': 'net_victory_total', 'search': True, 'show': False, 'title': _('net victory total')},
    'rtm': {'field': 'round_time_min', 'search': True, 'show': False, 'title': _('time min')},
    'rtom': {'field': 'round_time_min_over', 'search': True, 'show': False, 'title': _('round time min over')},
    'rtum': {'field': 'round_time_min_under', 'search': True, 'show': False, 'title': _('round time min under')},
    'rt': {'field': 'round_time', 'search': True, 'show': False, 'title': _('time average')},
    'rto': {'field': 'round_time_over', 'search': True, 'show': False, 'title': _('round time over')},
    'rtu': {'field': 'round_time_under', 'search': True, 'show': False, 'title': _('round time under')},
    'rtma': {'field': 'round_time_max', 'search': True, 'show': False, 'title': _('time max')},
    'rtoma': {'field': 'round_time_max_over', 'search': True, 'show': False, 'title': _('round time max over')},
    'rtuma': {'field': 'round_time_max_under', 'search': True, 'show': False, 'title': _('round time max under')},
    'to55': {'field': 'total_over_55', 'search': True, 'show': False, 'title': _('total over 5.5')},
    'tu55': {'field': 'total_under_55', 'search': True, 'show': False, 'title': _('total under 5.5')},
    'to65': {'field': 'total_over_65', 'search': True, 'show': False, 'title': _('total over 6.5')},
    'tu65': {'field': 'total_under_65', 'search': True, 'show': False, 'title': _('total under 6.5')},
    'to': {'field': 'total_over', 'search': True, 'show': False, 'title': _('total over')},
    'tu': {'field': 'total_under', 'search': True, 'show': False, 'title': _('total under')},
    'to85': {'field': 'total_over_85', 'search': True, 'show': False, 'title': _('total over 8.5')},
    'tu85': {'field': 'total_under_85', 'search': True, 'show': False, 'title': _('total under 8.5')},
    'fy': {'field': 'fatality_yes', 'search': True, 'show': False, 'title': _('fatality yes')},
    'fn': {'field': 'fatality_no', 'search': True, 'show': False, 'title': _('fatality no')},
    'itof15': {'field': 'individual_total_over_first_15', 'search': True, 'show': False, 'title': _('individual total over 1.5 first')},
    'ituf15': {'field': 'individual_total_under_first_15', 'search': True, 'show': False, 'title': _('individual total under 1.5 first')},
    'itos15': {'field': 'individual_total_over_second_15', 'search': True, 'show': False, 'title': _('individual total over 1.5 second')},
    'itus15': {'field': 'individual_total_under_second_15', 'search': True, 'show': False, 'title': _('individual total under 1.5 second')},
    'itof': {'field': 'individual_total_over_first', 'search': True, 'show': False, 'title': _('individual total over first')},
    'ituf': {'field': 'individual_total_under_first', 'search': True, 'show': False, 'title': _('individual total under first')},
    'itos': {'field': 'individual_total_over_second', 'search': True, 'show': False, 'title': _('individual total over second')},
    'itus': {'field': 'individual_total_under_second', 'search': True, 'show': False, 'title': _('individual total under second')},
    'itof35': {'field': 'individual_total_over_first_35', 'search': True, 'show': False, 'title': _('individual total over 3.5 first')},
    'ituf35': {'field': 'individual_total_under_first_35', 'search': True, 'show': False, 'title': _('individual total under 3.5 first')},
    'itos35': {'field': 'individual_total_over_second_35', 'search': True, 'show': False, 'title': _('individual total over 3.5 second')},
    'itus35': {'field': 'individual_total_under_second_35', 'search': True, 'show': False, 'title': _('individual total under 3.5 second')},
    'fto05': {'field': 'fatality_total_over_05', 'search': True, 'show': False, 'title': _('fatality total over 05')},
    'ftu05': {'field': 'fatality_total_under_05', 'search': True, 'show': False, 'title': _('fatality total under 05')},
    'bto05': {'field': 'brutality_total_over_05', 'search': True, 'show': False, 'title': _('brutality total over 05')},
    'btu05': {'field': 'brutality_total_under_05', 'search': True, 'show': False, 'title': _('brutality total under 05')},
    'fto25': {'field': 'fatality_total_over_25', 'search': True, 'show': False, 'title': _('fatality total over 25')},
    'ftu25': {'field': 'fatality_total_under_25', 'search': True, 'show': False, 'title': _('fatality total under 25')},
    'bto25': {'field': 'brutality_total_over_25', 'search': True, 'show': False, 'title': _('brutality total over 25')},
    'btu25': {'field': 'brutality_total_under_25', 'search': True, 'show': False, 'title': _('brutality total under 25')},
    'fto45': {'field': 'fatality_total_over_45', 'search': True, 'show': False, 'title': _('fatality total over 45')},
    'ftu45': {'field': 'fatality_total_under_45', 'search': True, 'show': False, 'title': _('fatality total under 45')},
    'bto45': {'field': 'brutality_total_over_45', 'search': True, 'show': False, 'title': _('brutality total over 45')},
    'btu45': {'field': 'brutality_total_under_45', 'search': True, 'show': False, 'title': _('brutality total under 45')},
}

GROUP_COLUMNS = [
    {
        'title': _('total'),
        'fields': [
            {'field': 't55', 'title': _(
                '5,5 over/5,5 under'), 'columns': ['to55', 'tu55']},
            {'field': 't65', 'title': _(
                '6,5 over/6,5 under'), 'columns': ['to65', 'tu65']},
            {'field': 't75', 'title': _(
                '7,5 over/7,5 under'), 'columns': ['to', 'tu']},
            {'field': 't85', 'title': _(
                '8,5 over/8,5 under'), 'columns': ['to85', 'tu85']},
        ]
    },
    {
        'title': _('other total'),
        'fields': [
            {'field': 'ft05', 'title': _(
                'fatality 0,5 over/0,5 under'), 'columns': ['fto05', 'ftu05']},
            {'field': 'bt05', 'title': _(
                'brutality 0,5 over/0,5 under'), 'columns': ['bto05', 'btu05']},
            {'field': 'ft25', 'title': _(
                'fatality 2,5 over/2,5 under'), 'columns': ['fto25', 'ftu25']},
            {'field': 'bt25', 'title': _(
                'brutality 2,5 over/2,5 under'), 'columns': ['bto25', 'btu25']},
            {'field': 'ft45', 'title': _(
                'fatality 4,5 over/4,5 under'), 'columns': ['fto45', 'ftu45']},
            {'field': 'bt45', 'title': _(
                'brutality 4,5 over/4,5 under'), 'columns': ['bto45', 'btu45']}
        ]
    },
    {
        'title': _('individual total first/second'),
        'fields': [
            {'field': 'it15', 'title': _(
                '1,5 over/1,5 under'), 'columns': ['itof15', 'ituf15', 'itos15', 'itus15']},
            {'field': 'it25', 'title': _(
                '2,5 over/2,5 under'), 'columns': ['itof', 'ituf', 'itos', 'itus']},
            {'field': 'it35', 'title': _(
                '3,5 over/3,5 under'), 'columns': ['itof35', 'ituf35', 'itos35', 'itus35']},
        ]
    },
    {
        'title': _('round time'),
        'fields': [
            {'field': 'rtm', 'title': _('min'), 'columns': [
                'rtm', 'rtom', 'rtum']},
            {'field': 'rta', 'title': _('average'), 'columns': [
                'rt', 'rto', 'rtu']},
            {'field': 'rtma', 'title': _('max'), 'columns': [
                'rtma', 'rtoma', 'rtuma']},
        ]
    },
    {
        'title': _('other'),
        'fields': [
            {'field': 'nvt', 'title': _(
                'net victory total'), 'columns': ['nvt']},
            {'field': 'fyn', 'title': _(
                'fatality yes/no'), 'columns': ['fy', 'fn']}
        ]
    },
]


DEFAULT_COLUMNS = {
    'f1x2w': {'field': 'first_1x2_win', 'search': True, 'show': True, 'title': _('first 1x2 win')},
    's1x2w': {'field': 'second_1x2_win', 'search': True, 'show': True, 'title': _('second 1x2 win')},
    'w1': {'field': 'first_win', 'search': True, 'show': True, 'title': _('first win')},
    'w2': {'field': 'second_win', 'search': True, 'show': True, 'title': _('second win')},
    'fat': {'field': 'fatality', 'search': True, 'show': True, 'title': _('fatality')},
    'bru': {'field': 'brutality', 'search': True, 'show': True, 'title': _('brutality')},
    'wf': {'field': 'without_finishing', 'search': True, 'show': True, 'title': _('without finishing')},
    'rb': {'field': 'result_brutality', 'search': True, 'show': True, 'title': _('result brutality')},
    'rf': {'field': 'result_fatality', 'search': True, 'show': True, 'title': _('result fatality')},
}

ROUND_COLUMS = {
    'rr1': {'field': 'result_round_1', 'round_search': True, 'show': True, 'title': _('result round 1')},
    'rr2': {'field': 'result_round_2', 'round_search': True, 'show': True, 'title': _('result round 2')},
    'rr3': {'field': 'result_round_3', 'round_search': True, 'show': True, 'title': _('result round 3')},
    'rr4': {'field': 'result_round_4', 'round_search': True, 'show': True, 'title': _('result round 4')},
    'rr5': {'field': 'result_round_5', 'round_search': True, 'show': True, 'title': _('result round 5')},
    'rr6': {'field': 'result_round_6', 'round_search': True, 'show': True, 'title': _('result round 6')},
    'rr7': {'field': 'result_round_7', 'round_search': True, 'show': True, 'title': _('result round 7')},
    'rr8': {'field': 'result_round_8', 'round_search': True, 'show': True, 'title': _('result round 8')},
    'rr9': {'field': 'result_round_9', 'round_search': True, 'show': True, 'title': _('result round 9')},
}
HIGHLIGHT_ROUND_LIST = {
    'hfb': _('fatality/brutality'),
    'how': _('outsider win'),
    'hwfs': _('win first/second'),
}


class StatListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    login_url = '/login/'
    redirect_field_name = 'next'
    queryset = Teams.objects.filter(is_active=True)
    search_query = {}
    columns = {}
    permission_required = 'mortalkombat.view_stat'
    template_name = 'mortalkombat/stat/stat.html'

    def __init__(self, *args, **kwargs):
        self.columns = copy.deepcopy(COLUMNS)
        super().__init__(*args, **kwargs)

    def get(self, request, *args, **kwargs):

        if self.get_query_string() == '' and self.request.session.get('query_string') is not None:
            return redirect(request.path + '?' + self.request.session.get('query_string'))
        elif self.get_query_string() != '':
            self.request.session['query_string'] = self.get_query_string()

        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):

        query_string = self.get_query_string()
        columns_list = self.request.GET.getlist('cl')
        columns = copy.deepcopy(COLUMNS)
        group_columns = copy.deepcopy(GROUP_COLUMNS)

        if columns_list:
            for group_columns_list in group_columns:
                for field in group_columns_list.get('fields', []):
                    if field.get('field') in columns_list:
                        field['checked'] = True
                        for column_key in field.get('columns', []):
                            columns[column_key]['show'] = True
                    else:
                        field['checked'] = False
                        for column_key in field.get('columns', []):
                            columns[column_key]['show'] = False
        else:
            columns = copy.deepcopy(COLUMNS)
            group_columns = copy.deepcopy(GROUP_COLUMNS)

        curent_language = translation.get_language()

        fighter_list = Fighter.objects.filter(is_active=True)
        if curent_language == 'ru':
            fighter_list = fighter_list.order_by('name')
        else:
            fighter_list = fighter_list.order_by('name_en')

        result_round_list = list(ResultRound.objects.filter(
            is_active=True).order_by('name').values('slug', 'name'))
        result_round_list = RESULT_ROUND_OTHER + result_round_list
        score_list = SCORE_LIST

        result_round_total_account = self.request.GET.get('rrta')
        filter_exactly = self.request.GET.get('fex')
        highlight_round = self.request.GET.get('hr', 'hfb')

        try:
            show_total_score = int(self.request.GET.get('sts', '0'))
        except ValueError:
            show_total_score = 0

        context = {
            'default_columns': DEFAULT_COLUMNS,
            'columns': columns,
            'group_columns': group_columns,
            'round_columns': ROUND_COLUMS,
            'query_string': query_string,
            'pp_list': STAT_PP_LIST,
            'total_score_list': TOTAL_SCORE_LIST,
            'cur_pp': self.get_paginate_by(None),
            'fighter_list': fighter_list,
            'result_round_list': result_round_list,
            'score_list': score_list,
            'search_query': self.search_query,
            'result_round_total_account': result_round_total_account,
            'filter_exactly': filter_exactly,
            'show_total_score': show_total_score,
            'highlight_round_list': HIGHLIGHT_ROUND_LIST,
            'highlight_round': highlight_round,
        }
        return super().get_context_data(**context)

    def get_query_string(self):
        query_string_list = []
        if self.request.META['QUERY_STRING']:
            search_query_keys = self.search_query.keys()
            for param in self.request.META['QUERY_STRING'].split('&'):
                if 'page' not in param and param not in search_query_keys:
                    query_string_list.append(param)
        query_string = '&'.join(query_string_list)
        return query_string

    def get_paginate_by(self, queryset):
        return self.request.GET.get('pp', STAT_PP)

    def get_queryset(self):

        queryset = super().get_queryset()
        queryset = queryset.annotate(total_score=F(
            'score_first') + F('score_second'))

        # get query params
        date_game = self.request.GET.get('dg')
        time_game = self.request.GET.get('tg')
        fighter_one = self.request.GET.get('f1')
        fighter_two = self.request.GET.get('f2')
        score_one = self.request.GET.get('s1')
        score_two = self.request.GET.get('s2')
        total_score_from = self.request.GET.get('tsf')
        total_score_to = self.request.GET.get('tst')
        filter_exactly = self.request.GET.get('fex')

        if total_score_from:
            try:
                total_score_from = int(total_score_from)
                queryset = queryset.filter(total_score__gte=total_score_from)
                self.search_query['tsf'] = total_score_from
            except ValueError:
                self.search_query['tsf'] = None
        else:
            self.search_query['tsf'] = None

        if total_score_to:
            try:
                total_score_to = int(total_score_to)
                queryset = queryset.filter(total_score__lte=total_score_to)
                self.search_query['tst'] = total_score_to
            except ValueError:
                self.search_query['tst'] = None
        else:
            self.search_query['tst'] = None

        if score_one:
            try:
                score_one = int(score_one)
                queryset = queryset.filter(score_first=score_one)
                self.search_query['s1'] = score_one
            except ValueError as e:
                self.search_query['s1'] = None
        else:
            self.search_query['s1'] = None

        if score_two:
            try:
                score_two = int(score_two)
                queryset = queryset.filter(score_second=score_two)
                self.search_query['s2'] = score_two
            except ValueError as e:
                self.search_query['s2'] = None
        else:
            self.search_query['s2'] = None

        if date_game:
            try:
                # Convert str date to datetime
                dategame = datetime.datetime.strptime(
                    date_game, '%d.%m.%y').date()
                # add to search_query
                queryset = queryset.filter(date_game__date=dategame)
                self.search_query['dg'] = date_game
            except ValueError as e:
                self.search_query['dg'] = None
        else:
            # add to search_query
            self.search_query['dg'] = None

        if time_game:
            try:
                # Convert str date to datetime
                timegame = datetime.datetime.strptime(
                    time_game, '%H:%M').time()
                # add to search_query
                queryset = queryset.filter(date_game__time=timegame)
                self.search_query['tg'] = time_game
            except ValueError as e:
                self.search_query['tg'] = None
        else:
            # add to search_query
            self.search_query['tg'] = None

        if fighter_one and fighter_one != 'None':
            queryset = queryset.filter(
                fighter_first__slug=fighter_one)
        self.search_query['f1'] = fighter_one

        if fighter_two and fighter_two != 'None':
            queryset = queryset.filter(
                fighter_second__slug=fighter_two)
        self.search_query['f2'] = fighter_two

        query = self.get_column_query(self.columns, filter_exactly)
        query.update(self.get_column_query(DEFAULT_COLUMNS, filter_exactly))

        for key, column in ROUND_COLUMS.items():
            q = self.request.GET.get('q_{}'.format(key))
            if q and q != 'None':
                other_query = [x['query']
                               for x in RESULT_ROUND_OTHER if x['slug'] == q]

                if len(other_query) > 0:
                    query['{}__name__icontains'.format(column["field"])] = other_query[0]
                else:
                    query['{}__slug'.format(column["field"])] = q

                self.search_query['q_{}'.format(key)] = q
            else:
                self.search_query['q_{}'.format(key)] = None

        if len(query):
            # print(query)
            queryset = queryset.filter(**query)

        return queryset

    def next_value(self, value):
        return decimal.Decimal(str(round(int(value * 10) / 10 + 0.1, 1)))

    def get_column_query(self, columns, filter_exactly):
        query = {}
        for key, column in columns.items():
            if column.get('search') is True:
                q = self.request.GET.get('q_{}'.format(key))
                if q:

                    if key == 'rb' or key == 'rf':
                        try:
                            value = int(q)
                        except ValueError as e:
                            continue
                        self.search_query['q_{}'.format(key)] = value
                        query[column["field"]] = value
                        continue

                    try:
                        value = decimal.Decimal(q.replace(',', '.'))
                    except ValueError as e:
                        continue

                    self.search_query['q_{}'.format(key)] = value

                    if filter_exactly == '1':
                        query['{}'.format(column["field"])] = value
                    else:
                        query['{}__gte'.format(column["field"])] = value
                        query['{}__lt'.format(column["field"])] = self.next_value(
                            value)
                else:
                    self.search_query['q_{}'.format(key)] = None
        return query
