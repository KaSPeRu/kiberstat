from django.apps import AppConfig


class MortalkombatConfig(AppConfig):
    name = 'mortalkombat'
