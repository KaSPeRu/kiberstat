from django.conf.urls import url
from mortalkombat.views import StatListView, StatPairView, ChartView

urlpatterns = [
    url(r'^$', StatListView.as_view(), name='stat-list'),
    url(r'^statpair/$', StatPairView.as_view(), name='stat-pair'),
    url(r'^charts/$', ChartView.as_view(), name='charts'),
]
