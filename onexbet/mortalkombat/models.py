import re
from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.

re_score = re.compile('(\d):(\d)')

class BaseModel(models.Model):
    name = models.CharField(_('name'), max_length=50)
    slug = models.SlugField(_('code'))
    is_active = models.BooleanField(_('is active'), default=True)
    created = models.DateTimeField(_("created"), auto_now_add=True)
    last_updated = models.DateTimeField(_("last updated"), auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Fighter(BaseModel):
    """Fighter model"""
    name_en = models.CharField(_('name en'), max_length=50, blank=True, null=True)
    objects = models.Manager()


class Sport(BaseModel):
    """Sport model"""
    sport_id = models.PositiveSmallIntegerField(_('sport id'), db_index=True)
    name_en = models.CharField(
        _('name en'), max_length=50, blank=True, null=True)


class ResultRound(models.Model):
    """Result round model"""
    name = models.CharField(_('name'), max_length=5)
    slug = models.SlugField(_('code'), max_length=5)
    is_active = models.BooleanField(_('is active'), default=True)
    created = models.DateTimeField(_("created"), auto_now_add=True)
    last_updated = models.DateTimeField(_("last updated"), auto_now=True)

    def __str__(self):
        return self.name


class TeamsManager(models.Manager):

    def get_total_win(self, teams_list, game_round, score):
        result = 0

        if game_round in range(1, 10):
            result = teams_list.filter(
                **{'result_round_{}__name__contains'.format(game_round): score}).count()

        return result

    def get_total_win_first(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '1:0')

    def get_total_win_second(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '0:1')

    def get_total_fatality_first(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '1:0 F')

    def get_total_fatality_second(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '0:1 F')

    def get_total_brutality_first(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '1:0 B')

    def get_total_brutality_second(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '0:1 B')

    def get_total_without_finishing_first(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '1:0 R')

    def get_total_without_finishing_second(self, teams_list, game_round):
        return self.get_total_win(teams_list, game_round, '0:1 R')

    def get_stat_pair(self, fighter_first_slug, fighter_second_slug):
        # import ipdb; ipdb.set_trace()
        result = {}
        total_all_round = 0
        total_fatality_all_round = 0
        total_brutality_all_round = 0
        total_without_finishing_all_round = 0


        try:
            fighter_first = Fighter.objects.get(slug=fighter_first_slug)
        except Fighter.DoesNotExist:
            return result, 0, {}

        try:
            fighter_second = Fighter.objects.get(slug=fighter_second_slug)
        except Fighter.DoesNotExist:
            return result, 0, {}

        teams_list = self.get_queryset().filter(
            fighter_first=fighter_first, fighter_second=fighter_second, finish=True)

        total = teams_list.count()

        # result['total'] = total

        if total == 0:
            return result, total, {}


        for game_round in range(1, 10):
            total_win_first = self.get_total_win_first(teams_list, game_round)
            total_win_second = self.get_total_win_second(teams_list, game_round)
            total_win = total_win_first + total_win_second
            total_percent = round(total_win / total * 100, 2)

            total_fatality_first = self.get_total_fatality_first(teams_list, game_round)
            total_fatality_second = self.get_total_fatality_second(teams_list, game_round)
            total_fatality = total_fatality_first + total_fatality_second
            if total_win > 0:
                total_fatality_percent = round(total_fatality/total_win * 100, 2)
                total_fatality_percent_first = round(
                    total_fatality_first / total_win * 100, 2)
                total_fatality_percent_second = round(
                    total_fatality_second / total_win * 100, 2)
            else:
                total_fatality_percent = 0
                total_fatality_percent_first = 0
                total_fatality_percent_second = 0

            total_brutality_first = self.get_total_brutality_first(teams_list, game_round)
            total_brutality_second = self.get_total_brutality_second(teams_list, game_round)
            total_brutality = total_brutality_first + total_brutality_second
            if total_win > 0:
                total_brutality_percent = round(total_brutality/total_win * 100, 2)
                total_brutality_percent_first = round(
                    total_brutality_first / total_win * 100, 2)
                total_brutality_percent_second = round(
                    total_brutality_second / total_win * 100, 2)
            else:
                total_brutality_percent = 0
                total_brutality_percent_first = 0
                total_brutality_percent_second = 0

            total_without_finishing_first = self.get_total_without_finishing_first(teams_list, game_round)
            total_without_finishing_second = self.get_total_without_finishing_second(teams_list, game_round)
            total_without_finishing = total_without_finishing_first + total_without_finishing_second
            if total_win > 0:
                total_without_finishing_percent = round(total_without_finishing/total_win * 100, 2)
                total_without_finishing_percent_first = round(
                    total_without_finishing_first / total_win * 100, 2)
                total_without_finishing_percent_second = round(
                    total_without_finishing_second / total_win * 100, 2)
            else:
                total_without_finishing_percent = 0
                total_without_finishing_percent_first = 0
                total_without_finishing_percent_second = 0


            total_all_round += total_win
            total_fatality_all_round += total_fatality
            total_brutality_all_round += total_brutality
            total_without_finishing_all_round += total_without_finishing


            result[game_round] = {
                'total_win_first': total_win_first,
                'total_win_second': total_win_second,
                'total_win': total_win,
                'total_percent': total_percent,
                'total_fatality_first': total_fatality_first,
                'total_fatality_second': total_fatality_second,
                'total_fatality': total_fatality,
                'total_fatality_percent': total_fatality_percent,
                'total_fatality_percent_first': total_fatality_percent_first,
                'total_fatality_percent_second': total_fatality_percent_second,
                'total_brutality_first': total_brutality_first,
                'total_brutality_second': total_brutality_second,
                'total_brutality': total_brutality,
                'total_brutality_percent': total_brutality_percent,
                'total_brutality_percent_first': total_brutality_percent_first,
                'total_brutality_percent_second': total_brutality_percent_second,
                'total_without_finishing_first': total_without_finishing_first,
                'total_without_finishing_second': total_without_finishing_second,
                'total_without_finishing': total_without_finishing,
                'total_without_finishing_percent': total_without_finishing_percent,
                'total_without_finishing_percent_first': total_without_finishing_percent_first,
                'total_without_finishing_percent_second': total_without_finishing_percent_second,
           }
        self.add_dye_color(result)
        return result, total, {
            'f': round(total_fatality_all_round/total_all_round * 100, 2),
            'b': round(total_brutality_all_round/total_all_round * 100, 2),
            'r': round(total_without_finishing_all_round/total_all_round * 100, 2),
        }

    def get_max_value_for_pair(self, stat_list, field):
        return max(stat_list.values(), key=lambda val: val[field]).get(field, 0)

    def get_color_range_for_field(self, max_value):
        range_dict = {}
        bit = max_value / 4
        bit2 = 0
        for key in range(1, 6):
            range_dict[key] = [bit2, bit2 + bit]
            bit2 = bit2 + bit
        return range_dict

    def get_css_class(self, value, field, stat_list):
        result = 'color_{}'.format(field)
        max_value = self.get_max_value_for_pair(stat_list, field)
        color_range_dict = self.get_color_range_for_field(max_value)

        for key, color_range in color_range_dict.items():
            if color_range[0] <= value < color_range[1]:
                result = '{}_{}'.format(result, key)

        return result

    def add_dye_color(self, stat_list):
        field_list = [
            'total_percent',
            'total_fatality_percent',
            'total_fatality_percent_first',
            'total_fatality_percent_second',
            'total_brutality_percent',
            'total_brutality_percent_first',
            'total_brutality_percent_second',
            'total_without_finishing_percent',
            'total_without_finishing_percent_first',
            'total_without_finishing_percent_second',
        ]
        for key, values in stat_list.items():
            for field in field_list:
                values['{}_css'.format(field)] = self.get_css_class(values[field], field, stat_list)

    def last_game(self):
        result = None

        queryset = self.get_queryset()
        item_list = queryset.filter(is_active=True).order_by('-date_game')

        if item_list.count() > 0:
            result = item_list[0]

        return result

class Teams(models.Model):
    """Teams model"""
    created = models.DateTimeField(_("created"), auto_now_add=True)
    last_updated = models.DateTimeField(_("last updated"), auto_now=True)
    is_active = models.BooleanField(_('is active'), default=True)
    game_id = models.PositiveIntegerField(_('game id'), db_index=True)
    number_id = models.PositiveIntegerField(_('number id'), db_index=True)
    video_id = models.CharField(_('video id'), max_length=10, db_index=True)
    date_game = models.DateTimeField(_('date game'), db_index=True)
    finish = models.BooleanField(_('finish game'), default=False)
    sport = models.ForeignKey(Sport, verbose_name=_('sport'), on_delete = models.CASCADE)
    fighter_first = models.ForeignKey(
        Fighter,
        on_delete = models.DO_NOTHING,
        related_name='fighter_first',
        verbose_name=_('first fighter'),
        )
    fighter_second = models.ForeignKey(
        Fighter,
        on_delete = models.DO_NOTHING,
        related_name='fighter_second',
        verbose_name=_('second fighter'),
        )
    score_first = models.PositiveSmallIntegerField(_('score first fighter'), default=0)
    score_second = models.PositiveSmallIntegerField(
        _('score second fighter'), default=0)

    net_victory_total = models.DecimalField(
        _('net victory total'), max_digits=10, decimal_places=3, blank=True, null=True)
    first_1x2_win = models.DecimalField(
        _('1x2 win 1'), max_digits=10, decimal_places=3, blank=True, null=True)
    second_1x2_win = models.DecimalField(
        _('1x2 win 2'), max_digits=10, decimal_places=3, blank=True, null=True)
    first_win = models.DecimalField(
        _('win 1'), max_digits=10, decimal_places=3, blank=True, null=True)
    second_win = models.DecimalField(
        _('win 2'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time = models.DecimalField(
        _('round time'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time_over = models.DecimalField(
        _('round time over'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time_under = models.DecimalField(
        _('round time under'), max_digits=10, decimal_places=3, blank=True, null=True)

    round_time_min = models.DecimalField(
        _('round time min'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time_min_over = models.DecimalField(
        _('round time min over'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time_min_under = models.DecimalField(
        _('round time min under'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time_max = models.DecimalField(
        _('round time max'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time_max_over = models.DecimalField(
        _('round time max over'), max_digits=10, decimal_places=3, blank=True, null=True)
    round_time_max_under = models.DecimalField(
        _('round time max under'), max_digits=10, decimal_places=3, blank=True, null=True)

    total_over = models.DecimalField(
        _('total over 7.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    total_under = models.DecimalField(
        _('total under 7.5'), max_digits=10, decimal_places=3, blank=True, null=True)

    total_over_55 = models.DecimalField(
        _('total over 5.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    total_under_55 = models.DecimalField(
        _('total under 5.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    total_over_65 = models.DecimalField(
        _('total over 6.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    total_under_65 = models.DecimalField(
        _('total under 6.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    total_over_85 = models.DecimalField(
        _('total over 8.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    total_under_85 = models.DecimalField(
        _('total under 8.5'), max_digits=10, decimal_places=3, blank=True, null=True)

    fatality = models.DecimalField(
        _('fatality'), max_digits=10, decimal_places=3, blank=True, null=True)
    brutality = models.DecimalField(
        _('brutality'), max_digits=10, decimal_places=3, blank=True, null=True)
    without_finishing = models.DecimalField(
        _('without finishing'), max_digits=10, decimal_places=3, blank=True, null=True)
    fatality_yes = models.DecimalField(
        _('fatality yes'), max_digits=10, decimal_places=3, blank=True, null=True)
    fatality_no = models.DecimalField(
        _('fatality no'), max_digits=10, decimal_places=3, blank=True, null=True)

    individual_total_over_first = models.DecimalField(
        _('individual total over first 2.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_under_first = models.DecimalField(
        _('individual total under first 2.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_over_second = models.DecimalField(
        _('individual total over second 2.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_under_second = models.DecimalField(
        _('individual total under second 2.5'), max_digits=10, decimal_places=3, blank=True, null=True)

    individual_total_over_first_15 = models.DecimalField(
        _('individual total over first 1.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_under_first_15 = models.DecimalField(
        _('individual total under first 1.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_over_second_15 = models.DecimalField(
        _('individual total over second 1.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_under_second_15 = models.DecimalField(
        _('individual total under second 1.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_over_first_35 = models.DecimalField(
        _('individual total over first 3.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_under_first_35 = models.DecimalField(
        _('individual total under first 3.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_over_second_35 = models.DecimalField(
        _('individual total over second 3.5'), max_digits=10, decimal_places=3, blank=True, null=True)
    individual_total_under_second_35 = models.DecimalField(
        _('individual total under second 3.5'), max_digits=10, decimal_places=3, blank=True, null=True)

    fatality_total_over_05 = models.DecimalField(
        _('fatality total over 05'), max_digits=10, decimal_places=3, blank=True, null=True)
    fatality_total_under_05 = models.DecimalField(
        _('fatality total under 05'), max_digits=10, decimal_places=3, blank=True, null=True)
    brutality_total_over_05 = models.DecimalField(
        _('brutality total over 05'), max_digits=10, decimal_places=3, blank=True, null=True)
    brutality_total_under_05 = models.DecimalField(
        _('brutality total under 05'), max_digits=10, decimal_places=3, blank=True, null=True)
    fatality_total_over_25 = models.DecimalField(
        _('fatality total over 25'), max_digits=10, decimal_places=3, blank=True, null=True)
    fatality_total_under_25 = models.DecimalField(
        _('fatality total under 25'), max_digits=10, decimal_places=3, blank=True, null=True)
    brutality_total_over_25 = models.DecimalField(
        _('brutality total over 25'), max_digits=10, decimal_places=3, blank=True, null=True)
    brutality_total_under_25 = models.DecimalField(
        _('brutality total under 25'), max_digits=10, decimal_places=3, blank=True, null=True)
    fatality_total_over_45 = models.DecimalField(
        _('fatality total over 45'), max_digits=10, decimal_places=3, blank=True, null=True)
    fatality_total_under_45 = models.DecimalField(
        _('fatality total under 45'), max_digits=10, decimal_places=3, blank=True, null=True)
    brutality_total_over_45 = models.DecimalField(
        _('brutality total over 45'), max_digits=10, decimal_places=3, blank=True, null=True)
    brutality_total_under_45 = models.DecimalField(
        _('brutality total under 45'), max_digits=10, decimal_places=3, blank=True, null=True)
    result_brutality = models.PositiveSmallIntegerField(
        _('result brutality'), default=0)
    result_fatality = models.PositiveSmallIntegerField(
        _('result fatality'), default=0)
    result = models.CharField(_('result'), max_length=3, blank=True, null=True)
    result_round_1 = models.ForeignKey(ResultRound, related_name='result_round_1', verbose_name=_(
        'result round 1'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_2 = models.ForeignKey(ResultRound, related_name='result_round_2', verbose_name=_(
        'result round 2'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_3 = models.ForeignKey(ResultRound, related_name='result_round_3', verbose_name=_(
        'result round 3'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_4 = models.ForeignKey(ResultRound, related_name='result_round_4', verbose_name=_(
        'result round 4'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_5 = models.ForeignKey(ResultRound, related_name='result_round_5', verbose_name=_(
        'result round 5'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_6 = models.ForeignKey(ResultRound, related_name='result_round_6', verbose_name=_(
        'result round 6'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_7 = models.ForeignKey(ResultRound, related_name='result_round_7', verbose_name=_(
        'result round 7'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_8 = models.ForeignKey(ResultRound, related_name='result_round_8', verbose_name=_(
        'result round 8'), blank=True, null=True, on_delete = models.CASCADE)
    result_round_9 = models.ForeignKey(ResultRound, related_name='result_round_9', verbose_name=_(
        'result round 9'), blank=True, null=True, on_delete = models.CASCADE)

    objects = TeamsManager()

    class Meta:
        ordering=['-date_game']
        permissions = (
                        ("view_stat", _("Can see statistics page")),
                        ("view_stat_pair", "Can see statistics pair page"),
                    )
    def score(self):
        return '{}:{}'.format(self.score_first, self.score_second)

    def is_win_first(self):
        return self.score_first > self.score_second

    def is_win_second(self):
        return self.score_first < self.score_second

    def has_brutality_first(self):
        result = False
        for key in range(1, 10):
            value = getattr(self, 'result_round_{}'.format(key))
            if value and '1:0 B' == value.name:
                result = True
        return result

    def has_brutality_second(self):
        result = False
        for key in range(1, 10):
            value = getattr(self, 'result_round_{}'.format(key))
            if value and '0:1 B' == value.name:
                result = True
        return result

    def has_win_first_round_first(self):
        return self.result_round_1 and '1:0' in self.result_round_1.name

    def has_win_first_round_second(self):
        return self.result_round_1 and '0:1' in self.result_round_1.name

    def canceled(self):
        return self.result_round_1 is None

    def has_r(self):
        result = False
        for key in range(1, 10):
            value = getattr(self, 'result_round_{}'.format(key))
            if value and 'R' in value.name:
                result = True
        return result
    def has_b(self):
        return self.result_brutality > 0

    def has_f(self):
        return self.result_fatality > 0

    def get_result_round_total_account(self, field):
        if not hasattr(self, field):
            return None

        if 'result_round' not in field:
            return None

        try:
            game_round = int(field.strip('result_round_'))
        except ValueError:
            return None

        if game_round > 9 or game_round <= 0:
            return None

        total_score_first = 0
        total_score_second = 0
        for bit in range(1, game_round + 1):
            round_field = 'result_round_{}'.format(bit)
            value = getattr(self, round_field)
            if value is None:
                return None
            score_first, score_second = re_score.findall(str(value))[0]
            try:
                score_first = int(score_first)
            except ValueError:
                score_first = 0
            try:
                score_second = int(score_second)
            except ValueError:
                score_second = 0

            total_score_first += score_first
            total_score_second += score_second

        value_type = str(getattr(self, field))[4:]

        return '{}:{} {}'.format(total_score_first, total_score_second, value_type)

    def outsider_win(self):
        if not self.first_1x2_win and not self.second_1x2_win:
            return False, ''
        elif self.first_1x2_win and not self.second_1x2_win and self.is_win_first():
            return True, '1:0'
        elif self.second_1x2_win and not self.first_1x2_win and self.is_win_second():
            return True, '0:1'
        elif self.first_1x2_win > self.second_1x2_win and self.is_win_first():
            return True, '1:0'
        elif self.second_1x2_win > self.first_1x2_win and self.is_win_second():
            return True, '0:1'
        else:
            return False, ''

    def outsider_win_in_round(self, game_round_value):
        is_outsider_win, win = self.outsider_win()

        if is_outsider_win and win in game_round_value:
            return True
        else:
            return False



class BetMirrorManager(models.Manager):
    def last_url(self):
        result = None
        queryset = self.get_queryset()
        item_list = queryset.filter(is_active=True).order_by('created')

        if item_list.count() > 0:
            result = item_list[0]

        return result


class BetMirror(models.Model):
    name = models.CharField(_('mirror name'), max_length=50)
    url = models.URLField(_('url'))
    is_active = models.BooleanField(_('is active'), default=True)
    created = models.DateTimeField(_("created"), auto_now_add=True)
    last_updated = models.DateTimeField(_("last updated"), auto_now=True)

    objects = BetMirrorManager()
