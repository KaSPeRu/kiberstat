from django.contrib import admin
from mortalkombat.models import Sport, Fighter, Teams, ResultRound, BetMirror


@admin.register(Sport)
class SportAdmin(admin.ModelAdmin):
    list_display = ('sport_id', 'name', 'name_en', 'slug', 'is_active')
    list_display_links = ('name', 'name_en')
    prepopulated_fields = {'slug':('name', )}
    list_filter = ('name', 'is_active')


@admin.register(ResultRound)
class ResultRoundAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'is_active')
    list_display_links = ('name', 'slug')
    prepopulated_fields = {'slug':('name', )}
    list_filter = ('name', 'is_active')


@admin.register(Fighter)
class FighterAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_en', 'slug', 'is_active')
    list_display_links = ('name', 'name_en')
    prepopulated_fields = {'slug':('name', )}
    list_filter = ('name', 'is_active')


@admin.register(Teams)
class TeamsAdmin(admin.ModelAdmin):
    list_display = ('game_id', 'date_game', 'score', 'fighter_first',
                    'fighter_second', 'finish', 'is_active')
    list_display_links = ('game_id', )
    list_filter = (
        'finish',
        'is_active',
        'date_game',
        'fighter_first',
        'fighter_second',
    )
    date_hierarchy = 'date_game'

    def score(self, obj):
        return ("%s:%s" % (obj.score_first, obj.score_second))
    score.short_description = 'Score'


@admin.register(BetMirror)
class BetMirrorAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'is_active', 'last_updated')
    list_display_links = ('name', 'url')
    list_filter = ('is_active', )
