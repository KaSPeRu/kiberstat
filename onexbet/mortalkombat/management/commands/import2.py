import csv
import datetime
import os
import json
import ipdb
import re
from os import listdir
from os.path import isfile, join
from django.core.management.base import BaseCommand, CommandError
from mortalkombat.models import Teams, Fighter, Sport, ResultRound
from slugify import slugify


class Command(BaseCommand):
    def read_csv(self, filepath):
        print(filepath)
        sport = Sport.objects.get(sport_id=103)
        with open(filepath, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
            gid = 0

            total = 0
            cur = 0

            for row in spamreader:
                gid += 1
                cur += 1

                print(cur, total)
                print(', '.join(row))

                game_id = gid
                number_id = gid
                video_id = gid
                score_bits = row[4].split(':')

                try:
                    fighter_first = Fighter.objects.get(name=row[5])
                except Fighter.DoesNotExist:
                    print(f'DoesNotExist {row[5]}')
                    continue

                try:
                    fighter_second = Fighter.objects.get(name=row[6])
                except Fighter.DoesNotExist:
                    print(f'DoesNotExist {row[6]}')
                    continue

                if row[8]:
                    net_victory_total = float(row[8].replace(',', '.'))
                else:
                    net_victory_total = 0

                if row[9]:
                    first_1x2_win = float(row[9].replace(',', '.'))
                else:
                    first_1x2_win = 0

                if row[10]:
                    second_1x2_win = float(row[10].replace(',', '.'))
                else:
                    second_1x2_win = 0

                if row[11]:
                    first_win = float(row[11].replace(',', '.'))
                else:
                    first_win = 0

                if row[12]:
                    second_win = float(row[12].replace(',', '.'))
                else:
                    second_win = 0

                if row[13]:
                    round_time = float(row[13].replace(',', '.'))
                else:
                    round_time = 0

                if row[14]:
                    round_time_over = float(row[14].replace(',', '.'))
                else:
                    round_time_over = 0

                if row[15]:
                    round_time_under = float(row[15].replace(',', '.'))
                else:
                    round_time_under = 0

                if row[16]:
                    total_over = float(row[16].replace(',', '.'))
                else:
                    total_over = 0

                if row[17]:
                    total_under = float(row[17].replace(',', '.'))
                else:
                    total_under = 0

                if row[18]:
                    fatality = float(row[18].replace(',', '.'))
                else:
                    fatality = 0

                if row[19]:
                    brutality = float(row[19].replace(',', '.'))
                else:
                    brutality = 0

                if row[20]:
                    without_finishing = float(row[20].replace(',', '.'))
                else:
                    without_finishing = 0

                if row[21]:
                    fatality_yes = float(row[21].replace(',', '.'))
                else:
                    fatality_yes = 0

                if row[22]:
                    fatality_no = float(row[22].replace(',', '.'))
                else:
                    fatality_no = 0

                if row[23]:
                    individual_total_over_first = float(
                        row[23].replace(',', '.'))
                else:
                    individual_total_over_first = 0

                if row[24]:
                    individual_total_under_first = float(
                        row[24].replace(',', '.'))
                else:
                    individual_total_under_first = 0

                if row[25]:
                    individual_total_over_second = float(
                        row[25].replace(',', '.'))
                else:
                    individual_total_over_second = 0

                if row[26]:
                    individual_total_under_second = float(
                        row[26].replace(',', '.'))
                else:
                    individual_total_under_second = 0

                if row[27]:
                    fatality_total_over_05 = float(row[27].replace(',', '.'))
                else:
                    fatality_total_over_05 = 0

                if row[28]:
                    fatality_total_under_05 = float(row[28].replace(',', '.'))
                else:
                    fatality_total_under_05 = 0

                if row[29]:
                    brutality_total_over_05 = float(row[29].replace(',', '.'))
                else:
                    brutality_total_over_05 = 0

                if row[30]:
                    brutality_total_under_05 = float(row[30].replace(',', '.'))
                else:
                    brutality_total_under_05 = 0

                if row[31]:
                    fatality_total_over_25 = float(row[31].replace(',', '.'))
                else:
                    fatality_total_over_25 = 0

                if row[32]:
                    fatality_total_under_25 = float(row[32].replace(',', '.'))
                else:
                    fatality_total_under_25 = 0

                if row[33]:
                    brutality_total_over_25 = float(row[33].replace(',', '.'))
                else:
                    brutality_total_over_25 = 0

                if row[34]:
                    brutality_total_under_25 = float(row[34].replace(',', '.'))
                else:
                    brutality_total_under_25 = 0

                if row[35]:
                    fatality_total_over_45 = float(row[35].replace(',', '.'))
                else:
                    fatality_total_over_45 = 0

                if row[36]:
                    fatality_total_under_45 = float(row[36].replace(',', '.'))
                else:
                    fatality_total_under_45 = 0

                if row[37]:
                    brutality_total_over_45 = float(row[37].replace(',', '.'))
                else:
                    brutality_total_over_45 = 0

                if row[38]:
                    brutality_total_under_45 = float(row[38].replace(',', '.'))
                else:
                    brutality_total_under_45 = 0


                # result_raw = row[41]
                result = ''
                result_round_dict = {}
                # if result_raw and 'Матч прерван' not in result_raw:
                #     result = result_raw[:3]
                #     m = re.search('\((.*?)\)', result_raw)
                #     bits = m.group(1).split(';')
                #     cnt = 1
                result_brutality = 0
                result_fatality = 0
                for cnt in range(1, 10):
                    bit = row[40 + cnt]

                    if 'B' in bit:
                        result_brutality += 1
                    if 'F' in bit:
                        result_fatality += 1

                    if not bit:
                        continue

                    result_round_dict[cnt], created = ResultRound.objects.get_or_create(
                        slug=slugify(bit),
                        defaults={
                            'name': bit
                        }
                    )
                #         cnt += 1

                item = {
                    'number_id': number_id,
                    'date_game': datetime.datetime.strptime(f'{row[2]} {row[3]}', '%d/%m/%y %H:%M'),
                    'score_first': score_bits[0],
                    'score_second': score_bits[1],
                    'fighter_first': fighter_first,
                    'fighter_second': fighter_second,
                    'video_id': video_id,
                    'finish': True,
                    'sport': sport,
                    'net_victory_total': net_victory_total,
                    'first_1x2_win': first_1x2_win,
                    'second_1x2_win': second_1x2_win,
                    'first_win': first_win,
                    'second_win': second_win,
                    'round_time': round_time,
                    'round_time_over': round_time_over,
                    'round_time_under': round_time_under,
                    'total_over': total_over,
                    'total_under': total_under,
                    'fatality': fatality,
                    'brutality': brutality,
                    'without_finishing': without_finishing,
                    'fatality_yes': fatality_yes,
                    'fatality_no': fatality_no,
                    'individual_total_over_first': individual_total_over_first,
                    'individual_total_under_first': individual_total_under_first,
                    'individual_total_over_second': individual_total_over_second,
                    'individual_total_under_second': individual_total_under_second,
                    'fatality_total_over_05': fatality_total_over_05,
                    'fatality_total_under_05': fatality_total_under_05,
                    'brutality_total_over_05': brutality_total_over_05,
                    'brutality_total_under_05': brutality_total_under_05,
                    'fatality_total_over_25': fatality_total_over_25,
                    'fatality_total_under_25': fatality_total_under_25,
                    'brutality_total_over_25': brutality_total_over_25,
                    'brutality_total_under_25': brutality_total_under_25,
                    'fatality_total_over_45': fatality_total_over_45,
                    'fatality_total_under_45': fatality_total_under_45,
                    'brutality_total_over_45': brutality_total_over_45,
                    'brutality_total_under_45': brutality_total_under_45,
                    'result_brutality': result_brutality,
                    'result_fatality': result_fatality,
                    'result': result,
                }

                for key, result_round in result_round_dict.items():
                    item[f'result_round_{key}'] = result_round

                teams, created = Teams.objects.get_or_create(
                    game_id=game_id,
                    defaults=item
                )

                print(game_id, item)

    def handle(self, *args, **options):
        print('Start import')
        dir_path = '../media'
        onlyfiles = [join(dir_path, f) for f in listdir(
            dir_path) if isfile(join(dir_path, f))]

        for filepath in onlyfiles:
            self.read_csv(filepath)
