# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-14 18:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mortalkombat', '0006_auto_20171207_1055'),
    ]

    operations = [
        migrations.CreateModel(
            name='BetMirror',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='mirror name')),
                ('url', models.URLField(verbose_name='url')),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('last_updated', models.DateTimeField(auto_now=True, verbose_name='last updated')),
            ],
        ),
    ]
