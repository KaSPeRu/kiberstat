from django.conf.urls import url
from myauth.views import MyLoginView, MyLogoutView, RequestAccessView
from django.views.generic.base import TemplateView

urlpatterns = [
    url(r'^login/$', MyLoginView.as_view(), name='login'),
    url(r'^logout/$', MyLogoutView.as_view(template_name='registration/logout.html'), name='logout'),
    url(r'^registration/access_request/$',
        RequestAccessView.as_view(), name='request-access'),
    url(r'^registration/successfully/$',
        TemplateView.as_view(template_name='registration/successfully.html'), name='request-access-successfully'),
]
