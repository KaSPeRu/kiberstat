import pprint
import datetime
from django.contrib import admin
from myauth.models import AuthLog
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User

@admin.register(AuthLog)
class AuthLogAdmin(admin.ModelAdmin):
    list_display = ('username', 'ip', 'date_log', 'status')
    readonly_fields = ('username', 'ip', 'date_log', 'status', 'error_message')
    date_hierarchy = 'date_log'
    list_filter = (
        'status',
    )
    search_fields= ('username', 'ip')


@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return pprint.pformat(obj.get_decoded()).replace('\n', '<br>\n')

    def _username(self, obj):
        username = None
        uid = obj.get_decoded().get('_auth_user_id')

        if uid is not None:
            try:
                user = User.objects.get(pk=uid)
                username = user.username
            except User.DoesNotExist:
                pass
        return username

    def _ip(self, obj):
        ip = obj.get_decoded().get('ip')
        return ip


    def _last_touch(self, obj):
        last_touch = obj.get_decoded().get('last_touch')
        if last_touch is not None:
            last_touch = datetime.datetime.strptime(
                last_touch, '%Y-%m-%d %H:%M:%S %z')
        return last_touch


    _session_data.allow_tags=True
    list_display = ['session_key', '_username', '_ip', '_last_touch', 'expire_date']
    readonly_fields = ['_session_data']
    exclude = ['session_data']
    date_hierarchy='expire_date'
