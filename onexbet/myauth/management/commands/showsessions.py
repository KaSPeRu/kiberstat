import pytz
import datetime
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    def handle(self, *args, **options):
        local_tz = pytz.timezone('Europe/Moscow')
        for session in Session.objects.all():
            uid = session.get_decoded().get('_auth_user_id')
            last_touch = session.get_decoded().get('last_touch')
            ip = session.get_decoded().get('ip')

            if last_touch is not None:
                last_touch = datetime.datetime.strptime(
                    last_touch, '%Y-%m-%d %H:%M:%S %z')
                last_touch = last_touch.replace(tzinfo=pytz.utc).astimezone(local_tz)
                last_touch = local_tz.normalize(last_touch)

            try:
                user = User.objects.get(pk=uid)
                print(session, user, ip, last_touch)
            except User.DoesNotExist:
                pass
