from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    def handle(self, *args, **options):
        for session in Session.objects.all():
            last_touch = session.get_decoded().get('last_touch')

            if last_touch is None:
                Session.objects.save(str(session), None, None)
