from django.contrib.auth.views import LoginView, LogoutView
from myauth.forms import MyAuthenticationForm, RequestAccessForm
from myauth.models import AuthLog
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic.edit import FormView

class MyLoginView(LoginView):
    form_class = MyAuthenticationForm

class MyLogoutView(LogoutView):

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        AuthLog.objects.logout(request.user.username, request)
        return super().dispatch(request, *args, **kwargs)

class RequestAccessView(FormView):
    template_name = 'registration/request_access.html'
    success_url = '/registration/successfully/'
    form_class = RequestAccessForm

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)