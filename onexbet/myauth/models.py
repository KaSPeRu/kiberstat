from datetime import datetime, timedelta
from django.db import models
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.conf import settings

from myauth.utility import get_client_ip

def get_all_logged_in_users_last_touch():
    # Query all non-expired sessions
    # use timezone.now() instead of datetime.now() in latest versions of Django
    sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_session_dict = {}

    # Build a list of user ids from that query
    for session in sessions:
        data = session.get_decoded()

        uid = data.get('_auth_user_id', None)

        if uid is None:
            continue

        last_touch = data.get('last_touch', None)

        if last_touch is None:
            last_touch = timezone.now()
        else:
            try:
                last_touch = datetime.strptime(
                    last_touch, '%Y-%m-%d %H:%M:%S %z')
            except ValueError:
                last_touch = timezone.now()

        user_session_dict[uid] = last_touch

    # Query all logged in users based on id list
    return user_session_dict


class LoginLogManager(models.Manager):

    def check_multi_login(self, user):
        # import ipdb; ipdb.set_trace()

        if user.is_superuser is True or user.has_perm('myauth.multi_login'):
            return True

        now = timezone.now()

        users_last_touch = get_all_logged_in_users_last_touch()

        if users_last_touch.get(str(user.id)) is None:
            return True

        last_touch = users_last_touch.get(str(user.id))

        if now - last_touch > timedelta(0, settings.AUTO_LOGOUT_DELAY * 60, 0):
            return True
        else:
            return False

    def success(self, username, request):
        login_log=self.model(
            username=username,
            ip=get_client_ip(request) or None,
            date_log=timezone.now(),
            status=1,
            error_message='Login success'
        )
        login_log.save()

    def fail(self, username, request, error_message):
        login_log = self.model(
            username=username,
            ip=get_client_ip(request) or None,
            date_log=timezone.now(),
            status=4,
            error_message=error_message
        )
        login_log.save()

    def logout(self, username, request):
        login_log = self.model(
            username=username,
            ip=get_client_ip(request) or None,
            date_log=timezone.now(),
            status=2,
            error_message='Logout success'
        )
        login_log.save()

    def auto_logout(self, username, request):
        login_log = self.model(
            username=username,
            ip=get_client_ip(request) or None,
            date_log=timezone.now(),
            status=3,
            error_message='Auto logout success'
        )
        login_log.save()

STATUS_CHOICES = (
    (1, _('authorized')),
    (2, _('logged out')),
    (3, _('auto logged out')),
    (4, _('access denied')),
)

class AuthLog(models.Model):
    username = models.CharField(_('username'), max_length=150)
    ip = models.GenericIPAddressField(_('IP'), blank=True, null=True)
    date_log=models.DateTimeField(_('log date'))
    status = models.PositiveSmallIntegerField(
        _('auth status'), default=1, choices=STATUS_CHOICES, db_index=True)
    error_message=models.TextField(_('error message'), blank=True, null=True)

    objects = LoginLogManager()

    class Meta:
        permissions = (
            ("multi_login", _("Can multi login to the site")),
        )


    def __str__(self):
        return '{} status {}'.format(self.username, self.status)
