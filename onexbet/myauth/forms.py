from django.contrib.auth.forms import AuthenticationForm
from myauth.models import AuthLog
from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.mail import mail_managers

class MyAuthenticationForm(AuthenticationForm):
    def clean(self):
        try:
            super().clean()
        except forms.ValidationError as e:
            AuthLog.objects.fail(self.cleaned_data.get(
                'username'), self.request, str(e))
            raise e
# Пользователь уже авторизован на сайте. Одновременное использование сайта на нескольких устройствах запрещено. Чтобы авторизоваться на этом устройстве, выйдите на предыдущем и повторите авторизацию. Если вы не авторизованы на других устройствах, обратитесь в службу поддержки.
        if AuthLog.objects.check_multi_login(self.user_cache) is False:
            AuthLog.objects.fail(self.user_cache.username, self.request, 'Multi login access denied')
            raise forms.ValidationError(_(
                    "The user %(username)s is already authorized on the site. "
                    "Simultaneous use of the site on several devices is prohibited. "
                    "To authorize on this device, exit on the previous and repeat "
                    "the authorization. If you are not authorized on other devices, "
                    "please waiting %(minutes)s minutes and repeat the authorization."),
                code='multi_login',
                params={
                    'username': self.user_cache.username,
                    'minutes': settings.AUTO_LOGOUT_DELAY},
            )

        AuthLog.objects.success(
            self.cleaned_data.get('username'), self.request)
        return self.cleaned_data

class RequestAccessForm(forms.Form):
    name = forms.CharField(label=_('name'), widget=forms.TextInput(attrs={'class': 'form-control'}), help_text=_('Input your name'))
    email = forms.EmailField(
        label=_('e-mail'), widget=forms.TextInput(attrs={'class': 'form-control'}), help_text=_('Input your email for communitation.'))
    message = forms.CharField(
        label=_('message'), widget=forms.Textarea(attrs={'class': 'form-control'}), required=False, help_text=_('Input your message, if you wont.'))

    def save(self):
        message = """
        Имя: {self.cleaned_data.get('name')}
        E-mail: {self.cleaned_data.get('email')}
        Сообщение:
        {self.cleaned_data.get('message')}
        --
        kiberstat
        """
        mail_managers(' Новый запрос доступа', message)
