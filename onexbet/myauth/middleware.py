from datetime import datetime, timedelta
from django.utils import timezone
from django.conf import settings
from django.contrib import auth
from django.http import HttpResponseRedirect
from myauth.models import AuthLog
from myauth.utility import get_client_ip

class AutoLogout(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        if not request.user.is_authenticated():
            # Can't log out if not logged in
            return self.get_response(request)

        if request.user.has_perm('myauth.multi_login') is True:
            request.session['last_touch'] = timezone.now(
            ).strftime('%Y-%m-%d %H:%M:%S %z')
            request.session['ip'] = get_client_ip(request)
            return self.get_response(request)

        last_touch = request.session.get('last_touch')
        if last_touch is not None:
            try:
                last_touch = datetime.strptime(
                    last_touch, '%Y-%m-%d %H:%M:%S %z')
            except:
                last_touch = timezone.now()
        else:
            request.session['last_touch'] = timezone.now(
            ).strftime('%Y-%m-%d %H:%M:%S %z')
            request.session['ip'] = get_client_ip(request)
            return self.get_response(request)

        if timezone.now() - last_touch > timedelta(0, settings.AUTO_LOGOUT_DELAY * 60, 0):
            username = request.user.username
            
            auth.logout(request)
            AuthLog.objects.auto_logout(username, request)
        else:
            request.session['last_touch'] = timezone.now().strftime('%Y-%m-%d %H:%M:%S %z')
            request.session['ip'] = get_client_ip(request)

        return self.get_response(request)
