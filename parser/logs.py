import logging
from settings import DEBUG

def get_default_logger(name=__name__, level=None, to_file=None):

    if level is None and DEBUG is True:
        level = logging.DEBUG
    elif level is None and DEBUG is False:
        level = logging.ERROR
    elif level is not None:
        level = level
    else:
        level = logging.FATAL


    logger = logging.getLogger(name)
    logger.setLevel(level)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(level)

    # create formatter
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s -
    # %(message)s')
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    if to_file:
        ch_file = logging.FileHandler(to_file)
        ch_file.setFormatter(formatter)
        logger.addHandler(ch_file)

    # add ch to logger
    logger.addHandler(ch)

    return logger
