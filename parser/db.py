from pymongo import MongoClient
from settings import MONGODB_HOST, MONGODB_PORT, MONGODB_DB

class SbetDb(object):
    def __init__(self, db=MONGODB_DB):
        self._client = MongoClient(MONGODB_HOST, MONGODB_PORT)
        self._db = self._client[db]

    def get_db(self):
        return self._db


db = SbetDb().get_db()