import logging
import os
import re
import sys
from datetime import datetime, timezone

import ipdb
import pytz
from grab import Grab
from grab.error import DataNotFound, GrabNetworkError, GrabTimeoutError

from db import db
from logs import get_default_logger
from slugify import slugify
from settings import ONEXBET_PATH

from parser import OneXBetBase, ResponceException

sys.path.append(ONEXBET_PATH)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onexbet.settings")
import django
django.setup()

from mortalkombat.models import Fighter, Sport, Teams, ResultRound

logger = get_default_logger(__name__, level=logging.ERROR, to_file='bet.log')


def clear(): return os.system('clear')


class MortalKombatOneXBet(OneXBetBase):

    sport_id = 103

    def process_item(self, item):
        teams, teams_model, new_teams = self.process_teamitem(
            item, sport_id=self.sport_id)
        if teams is None:
            return
        self.load_game(teams, teams_model, new_teams)

        if teams.get('finish', False) is True and teams.get('result_load', True) is False:
            self.get_result(teams, teams_model)

        self.print(teams)

    def process_teamitem(self, item, sport_id):
        # ipdb.set_trace()
        new_teams = False
        teams_id = item.get("I")
        teams = db.teams.find_one({'teams_id': teams_id})
        if teams is None:
            doc = {
                'teams_id': item.get("I"),
                'number_id': item.get("N"),
                # 'date': datetime.now(pytz.timezone('Europe/Moscow')),
                'date': datetime.fromtimestamp(item.get('S', 0)),
                'sport_id': sport_id,
                'sport_title_ru': item.get("L"),
                'sport_title_en': item.get("LE"),
                'teams_title_ru': '{} - {}'.format(item.get("O1"). item.get("O2")),
                'teams_title_en': '{} - {}'.format(item.get("O1E"), item.get("O2E")),
                'team1_ru': item.get("O1"),
                'team2_ru': item.get("O2"),
                'team1_en': item.get("O1E"),
                'team2_en': item.get("O2E"),
                'video_id': item.get('VI'),
                'score_1': int(item.get('SC', {}).get('FS', {}).get('S1', 0)),
                'score_2': int(item.get('SC', {}).get('FS', {}).get('S2', 0)),
                'first_simple_item': item,
                'current_simple_item': item,
                'finish': False,
                'result_load': False,
                'result_fatality': 0,
                'result_brutality': 0,

            }

            if doc['score_1'] != 0 and doc['score_2'] != 0:
                logger.debug(
                    'Skip teams {} {} score: {}:{}'.format(
                        doc.get("teams_id"),
                        doc.get("teams_title_ru"),
                        doc["score_1"],
                        doc["score_2"],
                    ))

                return None, None, new_teams
            db.teams.insert_one(doc)
            teams = doc
            new_teams = True
        else:
            score_1 = int(item.get('SC', {}).get('FS', {}).get('S1', 0))
            score_2 = int(item.get('SC', {}).get('FS', {}).get('S2', 0))
            teams['score_1'] = score_1
            teams['score_2'] = score_2
            teams['current_simple_item'] = item

            if item.get('F') is True:
                teams['finish'] = True
                logger.debug('finish')

            db.teams.update_one(
                {'_id': teams['_id']}, {'$set': teams})

            if teams.get('params') is None:
                new_teams = True

        sport, created = Sport.objects.get_or_create(
            sport_id=teams['sport_id'],
            defaults={
                'name': teams['sport_title_ru'],
                'name_en': teams['sport_title_en'],
                'slug': slugify(teams['sport_title_en']),
                'is_active': True,
            }
        )

        fighter_first, created = Fighter.objects.get_or_create(
            slug=slugify(teams['team1_en']),
            defaults={
                'name': teams['team1_ru'],
                'name_en': teams['team1_en'],
                'is_active': True,
            }
        )

        fighter_second, created = Fighter.objects.get_or_create(
            slug=slugify(teams['team2_en']),
            defaults={
                'name': teams['team2_ru'],
                'name_en': teams['team2_en'],
                'is_active': True,
            }
        )

        teams_model, created = Teams.objects.get_or_create(
            game_id=teams['teams_id'],
            defaults={
                'number_id': teams['number_id'],
                'video_id': teams['video_id'].replace('xgame', ''),
                'date_game': teams['date'],
                'finish': teams['finish'],
                'sport': sport,
                'fighter_first': fighter_first,
                'fighter_second': fighter_second,
                'score_first': teams['score_1'],
                'score_second': teams['score_2'],
            }
        )


        if new_teams is False:
            teams_model.score_first = teams['score_1']
            teams_model.score_second = teams['score_2']
            teams_model.finish = teams['finish']
            teams_model.save()

        # teams['model'] = teams_model
        logger.debug('Process item {} {}'.format(teams.get("teams_id"), teams.get("teams_title_ru")))
        return teams, teams_model, new_teams

    def load_game(self, teams, teams_model, new_teams):
        logger.debug('Load game for teams {} {}'.format(teams.get("teams_id"), teams.get("teams_title_ru")))
        logger.debug('New teams {}'.format(new_teams))

        url = self.get_game_url(teams.get('teams_id', 0))

        grab = self.g.clone()

        try:
            resp = grab.go(url)
        except GrabTimeoutError as exc:
            raise ResponceException(str(exc))

        if resp.code != 200:
            logger.error('Code {} url {}'.format(resp.code, url))
            raise ResponceException(resp.code)

        if not hasattr(resp, 'json'):
            logger.error('Can not load json from url {}'.format(url))
            raise ResponceException(500)

        if resp.json.get('Value') is not None:
            if new_teams is True:
                teams['current_item'] = resp.json.get('Value')
                self.get_params(teams)
                self.update_params(teams_model, teams)
            else:
                teams['current_item_new'] = resp.json.get('Value')

        db.teams.update_one(
            {'_id': teams['_id']}, {'$set': teams})

        # ipdb.set_trace()

    def print(self, teams):
        print(teams['teams_id'], teams['teams_title_ru'],
              teams.get('date'))
        print('Number', '\t', 'Score', '\t', 'Date', '\t\t', 'Time', '\t', 'Video id')
        print(teams['number_id'], '\t', teams['score_1'], ':', teams['score_2'], '\t',
              teams['date'].strftime("%d.%m.%Y"), '\t', teams['date'].strftime("%H:%M"), '\t', teams['video_id'].replace('xgame', ''))
        print('------------------------------------------------------------------')
        print('Win 1', '\t', 'Win 2', '\t', 'Time', '\t', 'Over', '\t', 'Under', '\t', '7,5 Over', '\t', '7,5 Under')
        print(
            teams.get('params', {}).get('win1', {}).get('rate'), '\t',
            teams.get('params', {}).get('win2', {}).get('rate'), '\t',
            teams.get('params', {}).get('round_time'), '\t',
            teams.get('params', {}).get('round_time1', {}).get('rate'), '\t',
            teams.get('params', {}).get('round_time2', {}).get('rate'), '\t',
            teams.get('params', {}).get(
                'totalover', {}).get('rate'), '\t', '\t',
            teams.get('params', {}).get('totalunder', {}).get('rate'), '\t',
            )
        print('------------------------------------------------------------------')
        print('Fatality', '\t', 'Brutality', '\t', 'Without finishing')
        print(
            teams.get('params', {}).get('fatality', {}).get('rate'), '\t', '\t',
            teams.get('params', {}).get(
                'brutality', {}).get('rate'), '\t', '\t',
            teams.get('params', {}).get(
                'without_finishing', {}).get('rate'), '\t',
        )

        if teams.get('result_load', False) is True:
            print('------------------------------------------------------------------')
            print('Result:', teams.get('result'))
            print('Result fatality:', teams.get('result_fatality'),
                  'Result brutality:', teams.get('result_brutality'))

        print('------------------------------------------------------------------')
        print('')

    def get_params(self, teams):
        """
            4062 Brutality over
            4063 Brutality under
            4060 Fatality over
            4061 Fatality under
            4064 Net wins over
            4065 Net wins under
            4055 Net victory yes
        """
        params = teams.get('current_item', {}).get('E')
        if teams.get('params') is None:
            teams['params'] = {}

        round_time = {
            '1': {},
            '2': {},
        }

        for param in params:
            if param.get('T') == 2140:
                teams['params'].setdefault('win1', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 2141:
                teams['params'].setdefault('win2', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 1:
                teams['params'].setdefault('1x2_win1', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 3:
                teams['params'].setdefault('1x2_win2', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 4929:
                teams['params'].setdefault('fatality_yes', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 4930:
                teams['params'].setdefault('fatality_no', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })

            for key in [5.5, 6.5, 7.5, 8.5]:
                param_key = str(key).replace('.', '-')
                if param.get('T') == 9 and param.get('P') == key:
                    teams['params'].setdefault('totalover', {}).update(
                        {
                            param_key: {
                                'rate': param.get('C'),
                                'key': param.get('P'),
                                'type': param.get('T'),
                            }
                        })
                if param.get('T') == 10 and param.get('P') == key:
                    teams['params'].setdefault('totalunder', {}).update(
                        {
                            param_key: {
                                'rate': param.get('C'),
                                'key': param.get('P'),
                                'type': param.get('T'),
                            }
                        })

            # if param.get('T') == 9 and param.get('P') == 7.5:
            #     teams['params'].setdefault('totalover', {}).update(
            #         {
            #             'rate': param.get('C'),
            #             'key': param.get('P'),
            #             'type': param.get('T'),
            #         })
            # if param.get('T') == 10 and param.get('P') == 7.5:
            #     teams['params'].setdefault('totalunder', {}).update(
            #         {
            #             'rate': param.get('C'),
            #             'key': param.get('P'),
            #             'type': param.get('T'),
            #         })
            # if param.get('T') == 11 and param.get('P') == 2.5:
            #     teams['params'].setdefault('individual_total_over_1', {}).update(
            #         {
            #             'rate': param.get('C'),
            #             'key': param.get('P'),
            #             'type': param.get('T'),
            #         })
            # if param.get('T') == 12 and param.get('P') == 2.5:
            #     teams['params'].setdefault('individual_total_under_1', {}).update(
            #         {
            #             'rate': param.get('C'),
            #             'key': param.get('P'),
            #             'type': param.get('T'),
            #         })
            # if param.get('T') == 13 and param.get('P') == 2.5:
            #     teams['params'].setdefault('individual_total_over_2', {}).update(
            #         {
            #             'rate': param.get('C'),
            #             'key': param.get('P'),
            #             'type': param.get('T'),
            #         })
            # if param.get('T') == 14 and param.get('P') == 2.5:
            #     teams['params'].setdefault('individual_total_under_2', {}).update(
            #         {
            #             'rate': param.get('C'),
            #             'key': param.get('P'),
            #             'type': param.get('T'),
            #         })

            for key in [1.5, 2.5, 3.5]:
                param_key = str(key).replace('.','-')
                if param.get('T') == 11 and param.get('P') == key:
                    teams['params'].setdefault('individual_total_over_1', {}).update(
                        {
                            param_key: {
                                'rate': param.get('C'),
                                'key': param.get('P'),
                                'type': param.get('T'),
                            }
                        })
                if param.get('T') == 12 and param.get('P') == key:
                    teams['params'].setdefault('individual_total_under_1', {}).update(
                        {
                            param_key: {
                                'rate': param.get('C'),
                                'key': param.get('P'),
                                'type': param.get('T'),
                            }
                        })
                if param.get('T') == 13 and param.get('P') == key:
                    teams['params'].setdefault('individual_total_over_2', {}).update(
                        {
                            param_key: {
                                'rate': param.get('C'),
                                'key': param.get('P'),
                                'type': param.get('T'),
                            }
                        })
                if param.get('T') == 14 and param.get('P') == key:
                    teams['params'].setdefault('individual_total_under_2', {}).update(
                        {
                            param_key: {
                                'rate': param.get('C'),
                                'key': param.get('P'),
                                'type': param.get('T'),
                            }
                        })

            if param.get('T') == 2170:
                bits = str(param.get('P')).split('.')
                p_key = round((param.get('P') - int(bits[0])) * 100, 2)
                round_time['1'].update({
                    p_key: {
                        'rate': param.get('C'),
                        'key': p_key,
                        'type': param.get('T'),
                    }
                })
            if param.get('T') == 2171:
                bits = str(param.get('P')).split('.')
                p_key = round((param.get('P') - int(bits[0])) * 100, 2)
                round_time['2'].update({
                    p_key: {
                        'rate': param.get('C'),
                        'key': p_key,
                        'type': param.get('T'),
                    }
                })

            if param.get('T') == 4057:
                teams['params'].setdefault('fatality', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 4058:
                teams['params'].setdefault('brutality', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 4059:
                teams['params'].setdefault('without_finishing', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })
            if param.get('T') == 4060:
                teams['params'].setdefault('fatality_total_over', {}).update(
                    {str(param.get('P')).replace('.', '-'): {
                        'rate': param.get('C'),
                        'key': param.get('P'),
                        'type': param.get('T'),
                    }})
            if param.get('T') == 4061:
                teams['params'].setdefault('fatality_total_under', {}).update(
                    {str(param.get('P')).replace('.', '-'): {
                        'rate': param.get('C'),
                        'key': param.get('P'),
                        'type': param.get('T'),
                    }})
            if param.get('T') == 4062:
                teams['params'].setdefault('brutality_total_over', {}).update(
                    {str(param.get('P')).replace('.', '-'): {
                        'rate': param.get('C'),
                        'key': param.get('P'),
                        'type': param.get('T'),
                    }})
            if param.get('T') == 4063:
                teams['params'].setdefault('brutality_total_under', {}).update(
                    {str(param.get('P')).replace('.', '-'): {
                        'rate': param.get('C'),
                        'key': param.get('P'),
                        'type': param.get('T'),
                    }})
            if param.get('T') == 4064:
                teams['params'].setdefault('netwins_total_over', {}).update(
                    {str(param.get('P')).replace('.', '-'): {
                        'rate': param.get('C'),
                        'key': param.get('P'),
                        'type': param.get('T'),
                    }})
            if param.get('T') == 4065:
                teams['params'].setdefault('netwins_total_under', {}).update(
                    {str(param.get('P')).replace('.', '-'): {
                        'rate': param.get('C'),
                        'key': param.get('P'),
                        'type': param.get('T'),
                    }})
            if param.get('T') == 4055:
                teams['params'].setdefault('netvictory_total_under', {}).update(
                    {
                        'rate': param.get('C'),
                        'type': param.get('T'),
                    })

        time_list_1 = round_time['1'].keys()
        time_list_2 = round_time['2'].keys()

        time_min_1 = min(time_list_1)
        time_max_1 = max(time_list_1)

        for key in time_list_1:

            if key == time_min_1:
                teams['params']['round_time_min'] = {
                    'key': key,
                    'over': round_time['1'][key],
                    'under': round_time['2'][key],
                }

            if key == time_max_1:
                teams['params']['round_time_max'] = {
                    'key': key,
                    'over': round_time['1'][key],
                    'under': round_time['2'][key],
                }

            if key != time_max_1 and key != time_min_1:
                teams['params']['round_time_avg'] = {
                    'key': key,
                    'over': round_time['1'][key],
                    'under': round_time['2'][key],
                }

            # if key == time_min_1 or key == time_max_1:
            #     continue
            # teams['params']['round_time'] = key
            # teams['params'].setdefault(
            #     'round_time1', {}).update(round_time['1'][key])
            # teams['params'].setdefault(
            #     'round_time2', {}).update(round_time['2'][key])

    def get_result(self, teams, teams_model):
        logger.debug('Request result {} {}'.format(teams.get("teams_id"), teams.get("teams_title_ru")))

        url = self.get_result_url()

        date = teams["date"].strftime("%Y-%m-%d")

        body = '{"Language":"ru"}{"Params":["' + date + '", null, 103, null, null, 180]}{"Vers":6}{"Adult": false}'

        grab = self.g.clone()
        grab.setup(headers={
            'x-requested-with': 'XMLHttpRequest',
            'content-type': 'application/json',
            })

        try:
            resp = grab.go(url, post=body)
        except GrabTimeoutError as exc:
            raise ResponceException(str(exc))

        if resp.code != 200:
            logger.error('Code {} url {}'.format(resp.code, url))
            raise ResponceException(resp.code)

        if not hasattr(resp, 'json'):
            logger.error('Can not load json from url {}'.format(url))
            raise ResponceException(500)

        if resp.json.get('Success') is not True:
            logger.error('Result not Success url {}'.format(url))
            raise ResponceException(500)

        data = resp.json.get('Data')

        # ipdb.set_trace()

        if len(data) == 0:
            logger.error('Data is None url {}'.format(url))
            raise ResponceException(500)

        for row in data[0].get('Elems', [{},])[0].get('Elems', []):
            head = row.get('Head')
            if not head:
                continue
            if head[0] == teams['teams_id']:
                teams['result'] = head[6]

                m = re.search('\((.*?)\)', teams['result'])
                bits = m.group(1).split(';')
                for bit in bits:
                    if 'F' in bit:
                        teams['result_fatality'] = teams.get('result_fatality', 0) + 1
                    if 'B' in bit:
                        teams['result_brutality'] = teams.get('result_brutality', 0) + 1

                teams['result_date'] = datetime.fromtimestamp(
                    head[7])
                teams['result_date_raw'] = head[7]
                teams['result_raw'] = head
                teams['result_load'] = True
                db.teams.update_one(
                    {'_id': teams['_id']}, {'$set': teams})

                self.update_result(teams, teams_model)

    def update_params(self, teams_model, teams):

        key_list = ['0-5', '2-5', '4-5']
        total_key_list = ['5-5', '6-5', '7-5', '8-5']
        itotal_key_list = ['1-5', '2-5', '3-5']
        fatality_total_over = {}
        fatality_total_under = {}
        brutality_total_over = {}
        brutality_total_under = {}
        total_over = {}
        total_under = {}
        itotal_over1 = {}
        itotal_under1 = {}
        itotal_over2 = {}
        itotal_under2 = {}

        for key in key_list:
            if teams.get('params', {}).get('fatality_total_over', {}).get(key) is not None:
                fatality_total_over[key] = teams.get('params', {}).get(
                    'fatality_total_over', {}).get(key).get('rate', 0)
            if teams.get('params', {}).get('fatality_total_under', {}).get(key) is not None:
                fatality_total_under[key] = teams.get('params', {}).get(
                    'fatality_total_under', {}).get(key).get('rate', 0)

            if teams.get('params', {}).get('brutality_total_over', {}).get(key) is not None:
                brutality_total_over[key] = teams.get('params', {}).get(
                    'brutality_total_over', {}).get(key).get('rate', 0)
            if teams.get('params', {}).get('brutality_total_under', {}).get(key) is not None:
                brutality_total_under[key] = teams.get('params', {}).get(
                    'brutality_total_under', {}).get(key).get('rate', 0)

        for key in total_key_list:
            if teams.get('params', {}).get('totalover', {}).get(key) is not None:
                total_over[key] = teams.get('params', {}).get(
                    'totalover', {}).get(key).get('rate', 0)
            if teams.get('params', {}).get('totalunder', {}).get(key) is not None:
                total_under[key] = teams.get('params', {}).get(
                    'totalunder', {}).get(key).get('rate', 0)

        for key in itotal_key_list:
            if teams.get('params', {}).get('individual_total_over_1', {}).get(key) is not None:
                itotal_over1[key] = teams.get('params', {}).get(
                    'individual_total_over_1', {}).get(key).get('rate', 0)
            if teams.get('params', {}).get('individual_total_under_1', {}).get(key) is not None:
                itotal_under1[key] = teams.get('params', {}).get(
                    'individual_total_under_1', {}).get(key).get('rate', 0)
            if teams.get('params', {}).get('individual_total_over_2', {}).get(key) is not None:
                itotal_over2[key] = teams.get('params', {}).get(
                    'individual_total_over_2', {}).get(key).get('rate', 0)
            if teams.get('params', {}).get('individual_total_under_2', {}).get(key) is not None:
                itotal_under2[key] = teams.get('params', {}).get(
                    'individual_total_under_2', {}).get(key).get('rate', 0)

        teams_model.net_victory_total = teams.get('params', {}).get(
            'netvictory_total_under', {}).get('rate', 0)
        teams_model.first_1x2_win = teams.get('params', {}).get(
            '1x2_win1', {}).get('rate', 0)
        teams_model.second_1x2_win = teams.get('params', {}).get(
            '1x2_win2', {}).get('rate', 0)
        teams_model.first_win = teams.get(
            'params', {}).get('win1', {}).get('rate', 0)
        teams_model.second_win = teams.get(
            'params', {}).get('win2', {}).get('rate', 0)

        teams_model.round_time = teams.get(
            'params', {}).get('round_time_avg', {}).get('key', 0)
        teams_model.round_time_over = teams.get(
            'params', {}).get('round_time_avg', {}).get('over', {}).get('rate', 0)
        teams_model.round_time_under = teams.get(
            'params', {}).get('round_time_avg', {}).get('under', {}).get('rate', 0)

        teams_model.round_time_min = teams.get(
            'params', {}).get('round_time_min', {}).get('key', 0)
        teams_model.round_time_min_over = teams.get(
            'params', {}).get('round_time_min', {}).get('over', {}).get('rate', 0)
        teams_model.round_time_min_under = teams.get(
            'params', {}).get('round_time_min', {}).get('under', {}).get('rate', 0)
        teams_model.round_time_max = teams.get(
            'params', {}).get('round_time_max', {}).get('key', 0)
        teams_model.round_time_max_over = teams.get(
            'params', {}).get('round_time_max', {}).get('over', {}).get('rate', 0)
        teams_model.round_time_max_under = teams.get(
            'params', {}).get('round_time_max', {}).get('under', {}).get('rate', 0)

        teams_model.total_over = total_over.get('7-5', 0)
        teams_model.total_under = total_under.get('7-5', 0)

        teams_model.total_over_55 = total_over.get('5-5', 0)
        teams_model.total_under_55 = total_under.get('5-5', 0)
        teams_model.total_over_65 = total_over.get('6-5', 0)
        teams_model.total_under_65 = total_under.get('6-5', 0)
        teams_model.total_over_85 = total_over.get('8-5', 0)
        teams_model.total_under_85 = total_under.get('8-5', 0)

        teams_model.fatality = teams.get('params', {}).get(
            'fatality', {}).get('rate', 0)
        teams_model.brutality = teams.get('params', {}).get(
            'brutality', {}).get('rate', 0)
        teams_model.without_finishing = teams.get('params', {}).get(
            'without_finishing', {}).get('rate', 0)
        teams_model.fatality_yes = teams.get('params', {}).get(
            'fatality_yes', {}).get('rate', 0)
        teams_model.fatality_no = teams.get('params', {}).get(
            'fatality_no', {}).get('rate', 0)

        teams_model.individual_total_over_first = itotal_over1.get('2-5', 0)
        teams_model.individual_total_under_first = itotal_under1.get('2-5', 0)
        teams_model.individual_total_over_second = itotal_over2.get('2-5', 0)
        teams_model.individual_total_under_second = itotal_under2.get('2-5', 0)

        teams_model.individual_total_over_first_15 = itotal_over1.get('1-5', 0)
        teams_model.individual_total_under_first_15 = itotal_under1.get('1-5', 0)
        teams_model.individual_total_over_second_15 = itotal_over2.get('1-5', 0)
        teams_model.individual_total_under_second_15 = itotal_under2.get('1-5', 0)
        teams_model.individual_total_over_first_35 = itotal_over1.get('3-5', 0)
        teams_model.individual_total_under_first_35 = itotal_under1.get('3-5', 0)
        teams_model.individual_total_over_second_35 = itotal_over2.get('3-5', 0)
        teams_model.individual_total_under_second_35 = itotal_under2.get('3-5', 0)

        teams_model.fatality_total_over_05 = fatality_total_over.get('0-5', 0)
        teams_model.fatality_total_under_05 = fatality_total_under.get('0-5', 0)
        teams_model.brutality_total_over_05 = brutality_total_over.get('0-5', 0)
        teams_model.brutality_total_under_05 = brutality_total_under.get('0-5', 0)

        teams_model.fatality_total_over_25 = fatality_total_over.get('2-5', 0)
        teams_model.fatality_total_under_25 = fatality_total_under.get('2-5', 0)
        teams_model.brutality_total_over_25 = brutality_total_over.get('2-5', 0)
        teams_model.brutality_total_under_25 = brutality_total_under.get('2-5', 0)

        teams_model.fatality_total_over_45 = fatality_total_over.get('4-5', 0)
        teams_model.fatality_total_under_45 = fatality_total_under.get('4-5', 0)
        teams_model.brutality_total_over_45 = brutality_total_over.get('4-5', 0)
        teams_model.brutality_total_under_45 = brutality_total_under.get('4-5', 0)
        teams_model.save()

    def update_result(self, teams, teams_model):
        m = re.search('\((.*?)\)', teams['result'])
        bits = m.group(1).split(';')
        result_round_dict = {}
        cnt = 1
        for bit in bits:
            result_round_dict[cnt], created = ResultRound.objects.get_or_create(
                slug=slugify(bit),
                defaults = {
                    'name': bit
                }
            )
            cnt += 1

        teams_model.result_brutality = teams.get('result_brutality', 0)
        teams_model.result_fatality = teams.get('result_fatality', 0)
        teams_model.result = teams.get('result', '   ')[:3]

        for key, result_round in result_round_dict.items():
            setattr(teams_model, 'result_round_{}'.format(key), result_round)
        teams_model.save()
