import logging
from datetime import datetime, timezone
from parser import OneXBetBase
from logs import get_default_logger
from db import db

logger = get_default_logger(__name__, level=logging.DEBUG, to_file='bet.log')


class TekkenOneXBet(OneXBetBase):
    sport_id = 145
