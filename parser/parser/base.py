import logging
from grab import Grab
from logs import get_default_logger
from db import db
from datetime import datetime, timezone
from settings import GAME_URL, RESULT_URL, SPORT_LIST
import requests


logger = get_default_logger(__name__, to_file='bet.log')


class BaseError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value


class ResponceException(BaseError):
    pass


class OneXBetBase(object):
    sport_list_url = SPORT_LIST
    game_url = GAME_URL
    result_url = RESULT_URL
    sport_id = 0

    def __init__(self):
        self.g = Grab()

    def get_sport_list_url(self):
        return self.sport_list_url.format(sport_id=self.sport_id)

    def get_game_url(self, team_id):
        return self.game_url.format(teams_id=team_id)

    def get_result_url(self):
        return self.result_url

    def get_sport_list(self):
        url = self.get_sport_list_url()
        logger.debug('Load {}'.format(url))
        # resp = self.g.go(url)
        resp = requests.get(url)
        # clear()

        if resp.status_code != 200:
            logger.error('Code {} url {}'.format(resp.code, url))
            raise ResponceException(resp.code)

        if not hasattr(resp, 'json'):
            logger.error('Can not load json from url {}'.format(url))
            raise ResponceException(500)

        if resp.json()['Success'] is not True:
            logger.error('Result not Success url {}'.format(url))
            raise ResponceException(500)

        if len(resp.json()['Value']) == 0:
            logger.error('Value is None url {}'.format(url))
            raise ResponceException(500)

        # for item in resp.json.get('Value', []):
        # logger.debug(resp.json())
        # for item in resp.json()["Value"]:
            # self.process_item(item)


        for item in resp.json()["Value"]:
            #
            logger.debug(item)

            sport_id = self.sport_id
            new_teams = False
            teams_id = item.get("I")
            teams = db.teams.find_one({'teams_id': teams_id})

            if teams is None:
                doc = {
                    'teams_id': item.get("I"),
                    'number_id': item.get("N"),
                    # 'date': datetime.now(pytz.timezone('Europe/Moscow')),
                    'date': datetime.fromtimestamp(item.get('S', 0)),
                    'sport_id': sport_id,
                    'sport_title_ru': item.get("L"),
                    'sport_title_en': item.get("LE"),
                    'league_id': item.get("LI"),
                    'teams_title_ru': '{} - {}'.format(item.get("O1"), item.get("O2")),
                    'teams_title_en': '{} - {}'.format(item.get("O1E"), item.get("O2E")),
                    'team1_ru': item.get("O1"),
                    'team2_ru': item.get("O2"),
                    'team1_en': item.get("O1E"),
                    'team2_en': item.get("O2E"),
                    'video_id': item.get('VI'),
                    'zone_id': item.get('ZP'),
                    'score_1': int(item.get('SC', {}).get('FS', {}).get('S1', 0)),
                    'score_2': int(item.get('SC', {}).get('FS', {}).get('S2', 0)),
                    'first_simple_item': item,
                    'current_simple_item': item,
                    'finish': False,
                    'result_load': False,
                }

                if doc['score_1'] != 0 and doc['score_2'] != 0:
                    logger.debug(
                        'Skip teams {} {} score: {}:{}'.format(doc.get("teams_id"), doc.get("teams_title_ru"), doc["score_1"], doc["score_2"]))
                    continue

                db.teams.insert_one(doc)
                teams = doc
                new_teams = True

            else:
                score_1 = int(item.get('SC', {}).get('FS', {}).get('S1', 0))
                score_2 = int(item.get('SC', {}).get('FS', {}).get('S2', 0))
                teams['score_1'] = score_1
                teams['score_2'] = score_2
                teams['current_simple_item'] = item

                if teams.get('video_id') is None:
                    teams['video_id'] = item.get('VI')

                if teams.get('zone_id') is None:
                    teams['zone_id'] = item.get('ZP')


                if item.get('F') is True:
                    teams['finish'] = True
                    logger.debug('finish')

                db.teams.update_one(
                    {'_id': teams['_id']}, {'$set': teams})

            logger.debug(
                'Process item sport_id: {} {} {} score {}:{}'.format(
                    teams.get("sport_id"),
                    teams.get("teams_id"),
                    teams.get("teams_title_ru"),
                    teams.get("score_1"),
                    teams.get("score_2"),
                ))

            if teams is None:
                continue

            logger.debug(
                'Load game for teams {} {}'.format(teams.get("teams_id"), teams.get("teams_title_ru")))
            logger.debug('New teams {}'.format(new_teams))

            url = self.get_game_url(teams.get('teams_id', 0))

            grab = self.g.clone()

            try:
                resps = grab.go(url)
            except GrabTimeoutError as exc:
                raise ResponceException(str(exc))

            if resps.code != 200:
                logger.error('Code {} url {}'.format(resps.code, url))
                raise ResponceException(resps.code)

            if not hasattr(resps, 'json'):
                logger.error('Can not load json from url {}'.format(url))
                raise ResponceException(500)

            if resps.json.get('Value') is not None:
                if new_teams is True:
                    teams['current_item'] = resps.json.get('Value')
                    self.process_params(teams)
                    # self.get_params(teams)
                    # self.update_params(teams_model, teams)
                else:
                    teams['current_item_new'] = resps.json.get('Value')

            db.teams.update_one(
                {'_id': teams['_id']}, {'$set': teams})

            if teams.get('finish', False) is True and teams.get('result_load', True) is False:
                #################################################################
                logger.debug(
                    'Request result {} {}'.format(teams.get("teams_id"), teams.get("teams_title_ru")))

                url = self.get_result_url()

                date = teams["date"].strftime("%Y-%m-%d")

                body = '{"Language":"ru"}{"Params":["' + date + \
                    '", null, '+ str(self.sport_id) +', null, null, 180]}{"Vers":6}{"Adult": false}'

                grab = self.g.clone()
                grab.setup(headers={
                    'x-requested-with': 'XMLHttpRequest',
                    'content-type': 'application/json',
                })

                try:
                    resps = grab.go(url, post=body)
                except GrabTimeoutError as exc:
                    raise ResponceException(str(exc))

                if resps.code != 200:
                    logger.error('Code {} url {}'.format(resps.code, url))
                    raise ResponceException(resps.code)

                if not hasattr(resp, 'json'):
                    logger.error('Can not load json from url {}'.format(url))
                    raise ResponceException(500)

                if resps.json.get('Success') is not True:
                    logger.error('Result not Success url {}'.format(url))
                    raise ResponceException(500)

                data_list = resps.json.get('Data')


                if len(data_list) == 0:
                    logger.error('Data is None url {}'.format(url))
                    raise ResponceException(500)

                for data in data_list:
                    for elements in data.get('Elems', [{}, ]):
                        for row in elements.get('Elems', []):
                            head = row.get('Head')
                            if not head:
                                continue

                            if head[0] == teams['teams_id']:
                                teams['result'] = head[6]

                                self.process_result(head, teams)
                                # m = re.search('\((.*?)\)', teams['result'])
                                # bits = m.group(1).split(';')
                                # for bit in bits:
                                #     if 'F' in bit:
                                #         teams['result_fatality'] = teams.get(
                                #             'result_fatality', 0) + 1
                                #     if 'B' in bit:
                                #         teams['result_brutality'] = teams.get(
                                #             'result_brutality', 0) + 1

                                teams['result_date'] = datetime.fromtimestamp(
                                    head[7])
                                teams['result_date_raw'] = head[7]
                                teams['result_raw'] = head
                                teams['result_load'] = True
                                db.teams.update_one(
                                    {'_id': teams['_id']}, {'$set': teams})
                                ######################################################



    def process_item(self, item):
        logger.debug('aaa')



        if teams is None:
            return
        self.load_game(teams, new_teams)

        if teams.get('finish', False) is True and teams.get('result_load', True) is False:
            self.get_result(teams)

        # self.print(teams)

    def process_teamitem(self, item):
        # ipdb.set_trace()
        logger.debug(item)

        sport_id = self.sport_id
        new_teams = False
        teams_id = item.get("I")
        teams = db.teams.find_one({'teams_id': teams_id})

        if teams is None:
            doc = {
                'teams_id': item.get("I"),
                'number_id': item.get("N"),
                # 'date': datetime.now(pytz.timezone('Europe/Moscow')),
                'date': datetime.fromtimestamp(item.get('S', 0)),
                'sport_id': sport_id,
                'sport_title_ru': item.get("L"),
                'sport_title_en': item.get("LE"),
                'league_id': item.get("LI"),
                'teams_title_ru': '{} - {}'.format(item.get("O1"), item.get("O2")),
                'teams_title_en': '{} - {}'.format(item.get("O1E"), item.get("O2E")),
                'team1_ru': item.get("O1"),
                'team2_ru': item.get("O2"),
                'team1_en': item.get("O1E"),
                'team2_en': item.get("O2E"),
                'video_id': item.get('VI'),
                'zone_id': item.get('ZP'),
                'score_1': int(item.get('SC', {}).get('FS', {}).get('S1', 0)),
                'score_2': int(item.get('SC', {}).get('FS', {}).get('S2', 0)),
                'first_simple_item': item,
                'current_simple_item': item,
                'finish': False,
                'result_load': False,
            }

            if doc['score_1'] != 0 and doc['score_2'] != 0:
                logger.debug(
                    'Skip teams {} {} score: {}:{}'.format(doc.get("teams_id"), doc.get("teams_title_ru"), doc["score_1"], doc["score_2"]))

                return None, new_teams
            db.teams.insert_one(doc)
            teams = doc
            new_teams = True

        else:
            score_1 = int(item.get('SC', {}).get('FS', {}).get('S1', 0))
            score_2 = int(item.get('SC', {}).get('FS', {}).get('S2', 0))
            teams['score_1'] = score_1
            teams['score_2'] = score_2
            teams['current_simple_item'] = item

            if teams.get('video_id') is None:
                teams['video_id'] = item.get('VI')

            if teams.get('zone_id') is None:
                teams['zone_id'] = item.get('ZP')


            if item.get('F') is True:
                teams['finish'] = True
                logger.debug('finish')

            db.teams.update_one(
                {'_id': teams['_id']}, {'$set': teams})

        logger.debug(
            'Process item sport_id: {} {} {} score {}:{}'.format(
                teams.get("sport_id"),
                teams.get("teams_id"),
                teams.get("teams_title_ru"),
                teams.get("score_1"),
                teams.get("score_2"),
            ))
        return teams, new_teams

    def load_game(self, teams, new_teams):
        logger.debug(
            'Load game for teams {} {}'.format(teams.get("teams_id"), teams.get("teams_title_ru")))
        logger.debug('New teams {}'.format(new_teams))

        url = self.get_game_url(teams.get('teams_id', 0))

        grab = self.g.clone()

        try:
            resp = grab.go(url)
        except GrabTimeoutError as exc:
            raise ResponceException(str(exc))

        if resp.code != 200:
            logger.error('Code {} url {}'.format(resp.code, url))
            raise ResponceException(resp.code)

        if not hasattr(resp, 'json'):
            logger.error('Can not load json from url {}'.format(url))
            raise ResponceException(500)

        if resp.json.get('Value') is not None:
            if new_teams is True:
                teams['current_item'] = resp.json.get('Value')
                self.process_params(teams)
                # self.get_params(teams)
                # self.update_params(teams_model, teams)
            else:
                teams['current_item_new'] = resp.json.get('Value')

        db.teams.update_one(
            {'_id': teams['_id']}, {'$set': teams})

    def get_result(self, teams):
        logger.debug(
            'Request result {} {}'.format(teams.get("teams_id"), teams.get("teams_title_ru")))

        url = self.get_result_url()

        date = teams["date"].strftime("%Y-%m-%d")

        body = '{"Language":"ru"}{"Params":["' + date + \
            '", null, '+ str(self.sport_id) +', null, null, 180]}{"Vers":6}{"Adult": false}'

        grab = self.g.clone()
        grab.setup(headers={
            'x-requested-with': 'XMLHttpRequest',
            'content-type': 'application/json',
        })

        try:
            resp = grab.go(url, post=body)
        except GrabTimeoutError as exc:
            raise ResponceException(str(exc))

        if resp.code != 200:
            logger.error('Code {} url {}'.format(resp.code, url))
            raise ResponceException(resp.code)

        if not hasattr(resp, 'json'):
            logger.error('Can not load json from url {}'.format(url))
            raise ResponceException(500)

        if resp.json.get('Success') is not True:
            logger.error('Result not Success url {}'.format(url))
            raise ResponceException(500)

        data_list = resp.json.get('Data')


        if len(data_list) == 0:
            logger.error('Data is None url {}'.format(url))
            raise ResponceException(500)

        for data in data_list:
            for elements in data.get('Elems', [{}, ]):
                for row in elements.get('Elems', []):
                    head = row.get('Head')
                    if not head:
                        continue

                    if head[0] == teams['teams_id']:
                        teams['result'] = head[6]

                        self.process_result(head, teams)
                        # m = re.search('\((.*?)\)', teams['result'])
                        # bits = m.group(1).split(';')
                        # for bit in bits:
                        #     if 'F' in bit:
                        #         teams['result_fatality'] = teams.get(
                        #             'result_fatality', 0) + 1
                        #     if 'B' in bit:
                        #         teams['result_brutality'] = teams.get(
                        #             'result_brutality', 0) + 1

                        teams['result_date'] = datetime.fromtimestamp(
                            head[7])
                        teams['result_date_raw'] = head[7]
                        teams['result_raw'] = head
                        teams['result_load'] = True
                        db.teams.update_one(
                            {'_id': teams['_id']}, {'$set': teams})

    def process_result(self, result, teams):
        pass

    def process_params(self, teams):
        pass
