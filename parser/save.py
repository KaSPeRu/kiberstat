import csv
from db import db
from datetime import date, datetime, timedelta
# from onebet import OneBet
# import openpyxl
from openpyxl import Workbook
from settings import MEDIA_ROOT


def save_result(write_all: bool=False):
    print(write_all)
    if write_all is True:
        file_result = '{}/result_all.xlsx'.format(MEDIA_ROOT)
        docs = db.teams.find({'sport_id': 103})
    else:
        now = datetime.now()
        today = datetime(now.year, now.month, now.day, 0, 0, 0)
        yesterday = today - timedelta(1)

        print(yesterday, today)

        docs = db.teams.find({'sport_id': 103, 'date': {
            '$gte': yesterday,
            '$lt': today
        }})
        file_result = '{folder}/result_{date}.xlsx'.format(
            folder=MEDIA_ROOT, date=yesterday.strftime('%d-%m-%Y'))

    wb = Workbook()
    w_sheet = wb.active
    w_sheet.title = "1xbet"

    table_header = [
        'ID',
        '№',
        'Дата',
        'Время',
        'Счет',
        'Участник 1',
        'Участник 2',
        'ID видео',
        'ЧП',
        '1x2 П1',
        '1x2 П2',
        'П1',
        'П2',
        'Время меньше',
        'Б',
        'М',
        'Время среднее',
        'Б',
        'М',
        'Время больше',
        'Б',
        'М',
        '5,5 Б',
        '5,5 М',
        '6,5 Б',
        '6,5 М',
        '7,5 Б',
        '7,5 М',
        '8,5 Б',
        '8,5 М',
        'F',
        'B',
        'Бд',
        'F-Да',
        'F-Нет',
        'ИТ-1 1,5Б',
        'ИТ-1 1,5М',
        'ИТ-2 1,5Б',
        'ИТ-2 1,5М',
        'ИТ-1 2,5Б',
        'ИТ-1 2,5М',
        'ИТ-2 2,5Б',
        'ИТ-2 2,5М',
        'ИТ-1 3,5Б',
        'ИТ-1 3,5М',
        'ИТ-2 3,5Б',
        'ИТ-2 3,5М',
        'F(0,5)Б',
        'F(0,5)М',
        'B(0,5)Б',
        'B(0,5)М',
        'F(2,5)Б',
        'F(2,5)М',
        'B(2,5)Б',
        'B(2,5)М',
        'F(4,5)Б',
        'F(4,5)М',
        'B(4,5)Б',
        'B(4,5)М',
        'TB',
        'TF',
        'ИТОГ:',
    ]

    row_n = 1
    col_n = 1

    for t in table_header:
        w_sheet.cell(row=row_n, column=col_n).value = t
        col_n += 1

    row_n = 2

    key_list = ['0-5', '2-5', '4-5']
    total_key_list = ['5-5', '6-5', '7-5', '8-5']
    itotal_key_list = ['1-5', '2-5', '3-5']

    for doc in docs:

        fatality_total_over = {}
        fatality_total_under = {}
        brutality_total_over = {}
        brutality_total_under = {}
        total_over = {}
        total_under = {}
        itotal_over1 = {}
        itotal_under1 = {}
        itotal_over2 = {}
        itotal_under2 = {}

        for key in key_list:
            if doc.get('params', {}).get('fatality_total_over', {}).get(key) is not None:
                fatality_total_over[key] = str(doc.get('params', {}).get(
                    'fatality_total_over', {}).get(key).get('rate', '')).replace('.', ',')
            if doc.get('params', {}).get('fatality_total_under', {}).get(key) is not None:
                fatality_total_under[key] = str(doc.get('params', {}).get(
                    'fatality_total_under', {}).get(key).get('rate', '')).replace('.', ',')

            if doc.get('params', {}).get('brutality_total_over', {}).get(key) is not None:
                brutality_total_over[key] = str(doc.get('params', {}).get(
                    'brutality_total_over', {}).get(key).get('rate', '')).replace('.', ',')
            if doc.get('params', {}).get('brutality_total_under', {}).get(key) is not None:
                brutality_total_under[key] = str(doc.get('params', {}).get(
                    'brutality_total_under', {}).get(key).get('rate', '')).replace('.', ',')

        for key in total_key_list:
            if doc.get('params', {}).get('totalover', {}).get(key) is not None:
                total_over[key] = doc.get('params', {}).get(
                    'totalover', {}).get(key).get('rate', 0)
            if doc.get('params', {}).get('totalunder', {}).get(key) is not None:
                total_under[key] = doc.get('params', {}).get(
                    'totalunder', {}).get(key).get('rate', 0)

        for key in itotal_key_list:
            if doc.get('params', {}).get('individual_total_over_1', {}).get(key) is not None:
                itotal_over1[key] = doc.get('params', {}).get(
                    'individual_total_over_1', {}).get(key).get('rate', 0)
            if doc.get('params', {}).get('individual_total_under_1', {}).get(key) is not None:
                itotal_under1[key] = doc.get('params', {}).get(
                    'individual_total_under_1', {}).get(key).get('rate', 0)
            if doc.get('params', {}).get('individual_total_over_2', {}).get(key) is not None:
                itotal_over2[key] = doc.get('params', {}).get(
                    'individual_total_over_2', {}).get(key).get('rate', 0)
            if doc.get('params', {}).get('individual_total_under_2', {}).get(key) is not None:
                itotal_under2[key] = doc.get('params', {}).get(
                    'individual_total_under_2', {}).get(key).get('rate', 0)

        netvictory_total_under = str(doc.get('params', {}).get(
            'netvictory_total_under', {}).get('rate', '')).replace('.', ',')
        f1x2_win1 = str(doc.get('params', {}).get(
            '1x2_win1', {}).get('rate', '')).replace('.', ',')
        f1x2_win2 = str(doc.get('params', {}).get(
            '1x2_win2', {}).get('rate', '')).replace('.', ',')
        fatality_yes = str(doc.get('params', {}).get(
            'fatality_yes', {}).get('rate', '')).replace('.', ',')
        fatality_no = str(doc.get('params', {}).get(
            'fatality_no', {}).get('rate', '')).replace('.', ',')
        # individual_total_over_1 = str(doc.get('params', {}).get(
        #     'individual_total_over_1', {}).get('rate', '')).replace('.', ',')
        # individual_total_under_1 = str(doc.get('params', {}).get(
        #     'individual_total_under_1', {}).get('rate', '')).replace('.', ',')
        # individual_total_over_2 = str(doc.get('params', {}).get(
        #     'individual_total_over_2', {}).get('rate', '')).replace('.', ',')
        # individual_total_under_2 = str(doc.get('params', {}).get(
        #     'individual_total_under_2', {}).get('rate', '')).replace('.', ',')

        w_sheet.cell(row=row_n, column=1).value = doc.get('teams_id')
        w_sheet.cell(row=row_n, column=2).value = doc.get('number_id')
        w_sheet.cell(row=row_n, column=3).value = doc.get(
            'date').strftime("%d.%m.%Y")
        w_sheet.cell(row=row_n, column=4).value = doc.get(
            'date').strftime("%H:%M")
        w_sheet.cell(row=row_n, column=5).value = str(
            doc['score_1']) + ' : ' + str(doc['score_2'])
        w_sheet.cell(row=row_n, column=6).value = doc.get('team1_ru')
        w_sheet.cell(row=row_n, column=7).value = doc.get('team2_ru')
        w_sheet.cell(row=row_n, column=8).value = doc.get(
            'video_id').replace('xgame', '')
        w_sheet.cell(row=row_n, column=9).value = netvictory_total_under
        w_sheet.cell(row=row_n, column=10).value = f1x2_win1
        w_sheet.cell(row=row_n, column=11).value = f1x2_win2
        w_sheet.cell(row=row_n, column=12).value = str(doc.get(
            'params', {}).get('win1', {}).get('rate', '')).replace('.', ',')
        w_sheet.cell(row=row_n, column=13).value = str(doc.get(
            'params', {}).get('win2', {}).get('rate', '')).replace('.', ',')

        w_sheet.cell(row=row_n, column=14).value = str(
            doc.get('params', {}).get('round_time_min',
                                      {}).get('key', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=15).value = str(doc.get(
            'params', {}).get('round_time_min', {}).get('over', {}).get('rate', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=16).value = str(doc.get(
            'params', {}).get('round_time_min', {}).get('under', {}).get('rate', 0)).replace('.', ',')

        w_sheet.cell(row=row_n, column=17).value = str(
            doc.get('params', {}).get('round_time_avg',
                                      {}).get('key', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=18).value = str(doc.get(
            'params', {}).get('round_time_avg', {}).get('over', {}).get('rate', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=19).value = str(doc.get(
            'params', {}).get('round_time_avg', {}).get('under', {}).get('rate', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=20).value = str(
            doc.get('params', {}).get('round_time_max',
                                      {}).get('key', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=21).value = str(doc.get(
            'params', {}).get('round_time_max', {}).get('over', {}).get('rate', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=22).value = str(doc.get(
            'params', {}).get('round_time_max', {}).get('under', {}).get('rate', 0)).replace('.', ',')

        w_sheet.cell(row=row_n, column=23).value = str(
            total_over.get('5-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=24).value = str(
            total_under.get('5-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=25).value = str(
            total_over.get('6-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=26).value = str(
            total_under.get('6-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=27).value = str(
            total_over.get('7-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=28).value = str(
            total_under.get('7-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=29).value = str(
            total_over.get('8-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=30).value = str(
            total_under.get('8-5', 0)).replace('.', ',')

        w_sheet.cell(row=row_n, column=31).value = str(
            doc.get('params', {}).get('fatality', {}).get('rate', '')).replace('.', ',')
        w_sheet.cell(row=row_n, column=32).value = str(
            doc.get('params', {}).get('brutality', {}).get('rate', '')).replace('.', ',')
        w_sheet.cell(row=row_n, column=33).value = str(doc.get('params', {}).get(
            'without_finishing', {}).get('rate', '')).replace('.', ',')
        w_sheet.cell(row=row_n, column=34).value = fatality_yes
        w_sheet.cell(row=row_n, column=35).value = fatality_no

        w_sheet.cell(row=row_n, column=36).value = str(itotal_over1.get('1-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=37).value = str(itotal_under1.get('1-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=38).value = str(itotal_over2.get('1-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=39).value = str(itotal_under2.get('1-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=40).value = str(itotal_over1.get('2-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=41).value = str(itotal_under1.get('2-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=42).value = str(itotal_over2.get('2-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=43).value = str(itotal_under2.get('2-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=44).value = str(itotal_over1.get('3-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=45).value = str(itotal_under1.get('3-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=46).value = str(itotal_over2.get('3-5', 0)).replace('.', ',')
        w_sheet.cell(row=row_n, column=47).value = str(itotal_under2.get('3-5', 0)).replace('.', ',')

        w_sheet.cell(
            row=row_n, column=48).value = fatality_total_over.get('0-5')
        w_sheet.cell(
            row=row_n, column=49).value = fatality_total_under.get('0-5')
        w_sheet.cell(
            row=row_n, column=50).value = brutality_total_over.get('0-5')
        w_sheet.cell(
            row=row_n, column=51).value = brutality_total_under.get('0-5')
        w_sheet.cell(
            row=row_n, column=52).value = fatality_total_over.get('2-5')
        w_sheet.cell(
            row=row_n, column=53).value = fatality_total_under.get('2-5')
        w_sheet.cell(
            row=row_n, column=54).value = brutality_total_over.get('2-5')
        w_sheet.cell(
            row=row_n, column=55).value = brutality_total_under.get('2-5')
        w_sheet.cell(
            row=row_n, column=56).value = fatality_total_over.get('4-5')
        w_sheet.cell(
            row=row_n, column=57).value = fatality_total_under.get('4-5')
        w_sheet.cell(
            row=row_n, column=58).value = brutality_total_over.get('4-5')
        w_sheet.cell(
            row=row_n, column=59).value = brutality_total_under.get('4-5')
        w_sheet.cell(row=row_n, column=60).value = doc.get(
            'result_brutality', '')
        w_sheet.cell(row=row_n, column=61).value = doc.get(
            'result_fatality', '')
        w_sheet.cell(row=row_n, column=62).value = doc.get('result', '')
        row_n += 1
    wb.save(file_result)


def save_result_csv():
    with open('result.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=';',
                                quotechar='"', quoting=csv.QUOTE_ALL)
        # spamwriter = csv.writer(csvfile, dialect='excel')
        spamwriter.writerow([
            'ID',
            '№',
            'Дата',
            'Время',
            'Счет',
            'Участник 1',
            'Участник 2',
            'ID видео',
            'ЧП',
            '1x2 П1',
            '1x2 П2',
            'П1',
            'П2',
            'Время',
            'Б',
            'М',
            '7,5 Б',
            '7,5 М',
            'F',
            'B',
            'Бд',
            'F-Да',
            'F-Нет',
            'ИТ-1 2,5Б',
            'ИТ-1 2,5М',
            'ИТ-2 2,5Б',
            'ИТ-2 2,5М',
            'F(0,5)Б',
            'F(0,5)М',
            'B(0,5)Б',
            'B(0,5)М',
            'F(2,5)Б',
            'F(2,5)М',
            'B(2,5)Б',
            'B(2,5)М',
            'F(4,5)Б',
            'F(4,5)М',
            'B(4,5)Б',
            'B(4,5)М',
            'TB',
            'TF',
            'ИТОГ:',
        ])

        key_list = ['0-5', '2-5', '4-5']

        for doc in db.teams.find({}):
            print(doc.get('teams_title_ru'))

            fatality_total_over = {}
            fatality_total_under = {}
            brutality_total_over = {}
            brutality_total_under = {}

            for key in key_list:
                if doc.get('params', {}).get('fatality_total_over', {}).get(key) is not None:
                    fatality_total_over[key] = doc.get('params', {}).get(
                        'fatality_total_over', {}).get(key).get('rate')
                if doc.get('params', {}).get('fatality_total_under', {}).get(key) is not None:
                    fatality_total_under[key] = doc.get('params', {}).get(
                        'fatality_total_under', {}).get(key).get('rate')

                if doc.get('params', {}).get('brutality_total_over', {}).get(key) is not None:
                    brutality_total_over[key] = doc.get('params', {}).get(
                        'brutality_total_over', {}).get(key).get('rate')
                if doc.get('params', {}).get('brutality_total_under', {}).get(key) is not None:
                    brutality_total_under[key] = doc.get('params', {}).get(
                        'brutality_total_under', {}).get(key).get('rate')

            netvictory_total_under = str(doc.get('params', {}).get(
                'netvictory_total_under', {}).get('rate', '')).replace('.', ',')
            f1x2_win1 = str(doc.get('params', {}).get(
                '1x2_win1', {}).get('rate', '')).replace('.', ',')
            f1x2_win2 = str(doc.get('params', {}).get(
                '1x2_win2', {}).get('rate', '')).replace('.', ',')
            fatality_yes = str(doc.get('params', {}).get(
                'fatality_yes', {}).get('rate', '')).replace('.', ',')
            fatality_no = str(doc.get('params', {}).get(
                'fatality_no', {}).get('rate', '')).replace('.', ',')
            individual_total_over_1 = str(doc.get('params', {}).get(
                'individual_total_over_1', {}).get('rate', '')).replace('.', ',')
            individual_total_under_1 = str(doc.get('params', {}).get(
                'individual_total_over_1', {}).get('rate', '')).replace('.', ',')
            individual_total_over_2 = str(doc.get('params', {}).get(
                'individual_total_over_1', {}).get('rate', '')).replace('.', ',')
            individual_total_under_2 = str(doc.get('params', {}).get(
                'individual_total_over_1', {}).get('rate', '')).replace('.', ',')

            spamwriter.writerow([
                doc.get('teams_id'),
                doc.get('number_id'),
                doc.get('date').strftime("%d.%m.%Y"),
                doc.get('date').strftime("%H:%M"),
                str(doc['score_1']) + '-' + str(doc['score_2']),
                doc.get('team1_ru'),
                doc.get('team2_ru'),
                doc.get('video_id').replace('xgame', ''),
                netvictory_total_under,
                f1x2_win1,
                f1x2_win2,
                doc.get('params', {}).get('win1', {}).get('rate'),
                doc.get('params', {}).get('win2', {}).get('rate'),
                doc.get('params', {}).get('round_time'),
                doc.get('params', {}).get('round_time1', {}).get('rate'),
                doc.get('params', {}).get('round_time2', {}).get('rate'),
                doc.get('params', {}).get('totalover', {}).get('rate'),
                doc.get('params', {}).get('totalunder', {}).get('rate'),
                doc.get('params', {}).get('fatality', {}).get('rate'),
                doc.get('params', {}).get('brutality', {}).get('rate'),
                doc.get('params', {}).get('without_finishing', {}).get('rate'),
                fatality_yes,
                fatality_no,
                individual_total_over_1,
                individual_total_under_1,
                individual_total_over_2,
                individual_total_under_2,
                fatality_total_over.get('0-5'),
                fatality_total_under.get('0-5'),
                brutality_total_over.get('0-5'),
                brutality_total_under.get('0-5'),
                fatality_total_over.get('2-5'),
                fatality_total_under.get('2-5'),
                brutality_total_over.get('2-5'),
                brutality_total_under.get('2-5'),
                fatality_total_over.get('4-5'),
                fatality_total_under.get('4-5'),
                brutality_total_over.get('4-5'),
                brutality_total_under.get('4-5'),
                doc.get('result_brutality'),
                doc.get('result_fatality'),
                doc.get('result'),
            ])
