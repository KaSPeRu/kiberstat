MEDIA_ROOT = './media'

MONGODB_HOST = '192.168.1.100'
MONGODB_PORT = 27017
MONGODB_DB = 'onebet'

# Path for project
ONEXBET_PATH = '/home/aleksey_su/workspace/betstat/onexbet'

SPORT_LIST = 'https://1xstavka.ru/LiveFeed/Get1x2_Zip?sports={sport_id}&count=50&mode=4&country=1'
GAME_URL = 'https://1xstavka.ru/LiveFeed/GetGameZip?id={teams_id}&lng=ru&cfview=0&isSubGames=true'
RESULT_URL = 'https://1xstavka.ru/getTranslate/ViewGameResultsGroup'

DEBUG = True
