import os
import sys

from db import db
from settings import ONEXBET_PATH

sys.path.append(ONEXBET_PATH)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onexbet.settings")
import django
django.setup()

from mortalkombat.models import Teams


def get_params(teams):
    """
        4062 Brutality over
        4063 Brutality under
        4060 Fatality over
        4061 Fatality under
        4064 Net wins over
        4065 Net wins under
        4055 Net victory yes
    """
    params = teams.get('current_item', {}).get('E')
    if teams.get('params') is None:
        teams['params'] = {}

    round_time = {
        '1': {},
        '2': {},
    }

    if params is None:
        return

    for param in params:
        for key in [5.5, 6.5, 7.5, 8.5]:
            param_key = str(key).replace('.', '-')
            if param.get('T') == 9 and param.get('P') == key:
                teams['params'].setdefault('totalover', {}).update(
                    {
                        param_key: {
                            'rate': param.get('C'),
                            'key': param.get('P'),
                            'type': param.get('T'),
                        }
                    })
            if param.get('T') == 10 and param.get('P') == key:
                teams['params'].setdefault('totalunder', {}).update(
                    {
                        param_key: {
                            'rate': param.get('C'),
                            'key': param.get('P'),
                            'type': param.get('T'),
                        }
                    })

        for key in [1.5, 2.5, 3.5]:
            param_key = str(key).replace('.', '-')
            if param.get('T') == 11 and param.get('P') == key:
                teams['params'].setdefault('individual_total_over_1', {}).update(
                    {
                        param_key: {
                            'rate': param.get('C'),
                            'key': param.get('P'),
                            'type': param.get('T'),
                        }
                    })
            if param.get('T') == 12 and param.get('P') == key:
                teams['params'].setdefault('individual_total_under_1', {}).update(
                    {
                        param_key: {
                            'rate': param.get('C'),
                            'key': param.get('P'),
                            'type': param.get('T'),
                        }
                    })
            if param.get('T') == 13 and param.get('P') == key:
                teams['params'].setdefault('individual_total_over_2', {}).update(
                    {
                        param_key: {
                            'rate': param.get('C'),
                            'key': param.get('P'),
                            'type': param.get('T'),
                        }
                    })
            if param.get('T') == 14 and param.get('P') == key:
                teams['params'].setdefault('individual_total_under_2', {}).update(
                    {
                        param_key: {
                            'rate': param.get('C'),
                            'key': param.get('P'),
                            'type': param.get('T'),
                        }
                    })
        if param.get('T') == 2170:
            bits = str(param.get('P')).split('.')
            p_key = round((param.get('P') - int(bits[0])) * 100, 2)
            round_time['1'].update({
                p_key: {
                    'rate': param.get('C'),
                    'key': p_key,
                    'type': param.get('T'),
                }
            })
        if param.get('T') == 2171:
            bits = str(param.get('P')).split('.')
            p_key = round((param.get('P') - int(bits[0])) * 100, 2)
            round_time['2'].update({
                p_key: {
                    'rate': param.get('C'),
                    'key': p_key,
                    'type': param.get('T'),
                }
            })

    time_list_1 = round_time['1'].keys()
    time_list_2 = round_time['2'].keys()

    time_min_1 = min(time_list_1)
    time_max_1 = max(time_list_1)

    for key in time_list_1:

        if key == time_min_1:
            teams['params']['round_time_min'] = {
                'key': key,
                'over': round_time['1'][key],
                'under': round_time['2'][key],
            }

        if key == time_max_1:
            teams['params']['round_time_max'] = {
                'key': key,
                'over': round_time['1'][key],
                'under': round_time['2'][key],
            }

        if key != time_max_1 and key != time_min_1:
            teams['params']['round_time_avg'] = {
                'key': key,
                'over': round_time['1'][key],
                'under': round_time['2'][key],
            }



def update_params(teams_model, teams):
    key_list = ['0-5', '2-5', '4-5']
    total_key_list = ['5-5', '6-5', '7-5', '8-5']
    itotal_key_list = ['1-5', '2-5', '3-5']
    fatality_total_over = {}
    fatality_total_under = {}
    brutality_total_over = {}
    brutality_total_under = {}
    total_over = {}
    total_under = {}
    itotal_over1 = {}
    itotal_under1 = {}
    itotal_over2 = {}
    itotal_under2 = {}

    for key in total_key_list:
        if teams.get('params', {}).get('totalover', {}).get(key) is not None:
            total_over[key] = teams.get('params', {}).get(
                'totalover', {}).get(key).get('rate', 0)
        if teams.get('params', {}).get('totalunder', {}).get(key) is not None:
            total_under[key] = teams.get('params', {}).get(
                'totalunder', {}).get(key).get('rate', 0)

    for key in itotal_key_list:
        if teams.get('params', {}).get('individual_total_over_1', {}).get(key) is not None:
            itotal_over1[key] = teams.get('params', {}).get(
                'individual_total_over_1', {}).get(key).get('rate', 0)
        if teams.get('params', {}).get('individual_total_under_1', {}).get(key) is not None:
            itotal_under1[key] = teams.get('params', {}).get(
                'individual_total_under_1', {}).get(key).get('rate', 0)
        if teams.get('params', {}).get('individual_total_over_2', {}).get(key) is not None:
            itotal_over2[key] = teams.get('params', {}).get(
                'individual_total_over_2', {}).get(key).get('rate', 0)
        if teams.get('params', {}).get('individual_total_under_2', {}).get(key) is not None:
            itotal_under2[key] = teams.get('params', {}).get(
                'individual_total_under_2', {}).get(key).get('rate', 0)

    teams_model.round_time = teams.get(
        'params', {}).get('round_time_avg', {}).get('key', 0)
    teams_model.round_time_over = teams.get(
        'params', {}).get('round_time_avg', {}).get('over', {}).get('rate', 0)
    teams_model.round_time_under = teams.get(
        'params', {}).get('round_time_avg', {}).get('under', {}).get('rate', 0)

    teams_model.round_time_min = teams.get(
        'params', {}).get('round_time_min', {}).get('key', 0)
    teams_model.round_time_min_over = teams.get(
        'params', {}).get('round_time_min', {}).get('over', {}).get('rate', 0)
    teams_model.round_time_min_under = teams.get(
        'params', {}).get('round_time_min', {}).get('under', {}).get('rate', 0)
    teams_model.round_time_max = teams.get(
        'params', {}).get('round_time_max', {}).get('key', 0)
    teams_model.round_time_max_over = teams.get(
        'params', {}).get('round_time_max', {}).get('over', {}).get('rate', 0)
    teams_model.round_time_max_under = teams.get(
        'params', {}).get('round_time_max', {}).get('under', {}).get('rate', 0)

    teams_model.total_over = total_over.get('7-5', 0)
    teams_model.total_under = total_under.get('7-5', 0)

    teams_model.total_over_55 = total_over.get('5-5', 0)
    teams_model.total_under_55 = total_under.get('5-5', 0)
    teams_model.total_over_65 = total_over.get('6-5', 0)
    teams_model.total_under_65 = total_under.get('6-5', 0)
    teams_model.total_over_85 = total_over.get('8-5', 0)
    teams_model.total_under_85 = total_under.get('8-5', 0)

    teams_model.individual_total_over_first = itotal_over1.get('2-5', 0)
    teams_model.individual_total_under_first = itotal_under1.get('2-5', 0)
    teams_model.individual_total_over_second = itotal_over2.get('2-5', 0)
    teams_model.individual_total_under_second = itotal_under2.get('2-5', 0)

    teams_model.individual_total_over_first_15 = itotal_over1.get('1-5', 0)
    teams_model.individual_total_under_first_15 = itotal_under1.get('1-5', 0)
    teams_model.individual_total_over_second_15 = itotal_over2.get('1-5', 0)
    teams_model.individual_total_under_second_15 = itotal_under2.get('1-5', 0)
    teams_model.individual_total_over_first_35 = itotal_over1.get('3-5', 0)
    teams_model.individual_total_under_first_35 = itotal_under1.get('3-5', 0)
    teams_model.individual_total_over_second_35 = itotal_over2.get('3-5', 0)
    teams_model.individual_total_under_second_35 = itotal_under2.get('3-5', 0)

    teams_model.save()


def update():
    docs = db.teams.find({'sport_id': 103})
    for teams in docs:
        print(teams.get('teams_id'), '->')
        try:
            teams_model = Teams.objects.get(game_id=teams.get('teams_id'))
            print('->', teams_model.game_id, teams_model.date_game)
            get_params(teams)
            db.teams.update_one(
                {'_id': teams['_id']}, {'$set': teams})
            update_params(teams_model, teams)
        except Teams.DoesNotExist:
            print('skiped', teams.get('teams_id'))
            continue
