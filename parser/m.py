#!/usr/bin/env python
import fire
import time
from parser import (MortalKombatOneXBet, ResponceException,
                    FifaOneXBet, HandballOneXBet, BasketballOneXBet, TekkenOneXBet)
from save import save_result
from update import update

class BetPipeline(object):

    def start_loop(self, parser):
        while True:
            parser.get_sport_list()
            time.sleep(20)

    def run(self):
        onebet = MortalKombatOneXBet()
        self.start_loop(onebet)

    def fifa(self):
        fifa = FifaOneXBet()
        self.start_loop(fifa)

    def handball(self):
        handball = HandballOneXBet()
        self.start_loop(handball)

    def basketball(self):
        basketball = BasketballOneXBet()
        self.start_loop(basketball)

    def tekken(self):
        tekken = TekkenOneXBet()
        self.start_loop(tekken)

    def save(self, write_all=False):
        save_result(write_all)

    def update(self):
        update()


def main():
    fire.Fire(BetPipeline)

if __name__ == '__main__':
    main()
